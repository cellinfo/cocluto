# cocluto : COmpute CLUster TOols

these are a set of common modules that are used in various compute cluster related code at Institut de Physique de Rennes


## how to test

```sh
(cocluto.venv) graffy@graffy-ws2:~/work/simpaweb/cocluto.git$ python3 -m unittest test.test_cocluto
```