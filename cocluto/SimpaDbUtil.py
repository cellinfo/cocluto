from typing import Union, List
from pathlib import Path
from enum import Enum
import logging
import MySQLdb  # sudo port install py-mysql; sudo apt install python-mysqldb or pip install mysqlclient
import time
import sys
if sys.version_info < (3, 0):
    import StringIO
else:
    from io import StringIO
import re
from .wol import wake_on_lan
import os
from .Util import execute_program, execute_command, log
import abc
import sqlite3
from .mysql2sqlite import mysql_to_sqlite
import subprocess


def is_machine_responding(machineName):
    (returnCode, stdout, stderr) = execute_program(['ping', '-o', '-t', '1', machineName])
    # log( 'is_machine_responding : result of command %s : %d' % (command, returnCode) )

    if returnCode == 0:
        return True
    else:
        # bMachineNameIsNotKnown = (returnCode == 68)
        bMachineIsNotResponding = (returnCode == 2)
        if bMachineIsNotResponding is False:
            bBUG_00000004_IS_STILL_ALIVE = True
            if bBUG_00000004_IS_STILL_ALIVE is True and returnCode == 142:
                log('is_machine_responding : bug00000004 Unexpected return code : returnCode=%d, stdout="%s", stderr="%s" , machineName = %s' % (returnCode, stdout, stderr, machineName))
                # don't stop the program until we understand bug00000004
            elif bBUG_00000004_IS_STILL_ALIVE is True and returnCode == -14:  # I had this error code on 07/09/2009 20:38 but I don't know yet what that means
                log('is_machine_responding : bug00000004 Unexpected return code : returnCode=%d, stdout="%s", stderr="%s" , machineName = %s' % (returnCode, stdout, stderr, machineName))
                # don't stop the program until we understand bug00000004
            else:
                log('is_machine_responding : Unexpected return code : returnCode=%d, stdout="%s", stderr="%s" , machineName = %s' % (returnCode, stdout, stderr, machineName))
                assert False
        return False


SqlQuery = str


class SqlTableField():
    '''description of a field of a sql table
    '''
    class Type(Enum):
        FIELD_TYPE_STRING = 0
        FIELD_TYPE_INT = 1
        FIELD_TYPE_FLOAT = 2
        FIELD_TYPE_TIME = 3

    name: str  # the name of the field, eg 'matrix_size'
    field_type: Type  # the type of the field, eg 'PARAM_TYPE_INT'
    description: str  # the description of the field, eg 'the size n of the n*n matrix '
    is_autoinc_index: bool  # indicates if theis field is used as an autoincrement index in the table

    def __init__(self, name: str, field_type: Type, description: str, is_autoinc_index=False):
        if is_autoinc_index:
            assert field_type == SqlTableField.Type.FIELD_TYPE_INT, 'only an integer field can be used as a autoincrement table index'
        self.name = name
        self.field_type = field_type
        self.description = description
        self.is_autoinc_index = is_autoinc_index


class ISqlDatabaseBackend(object):
    def __init__(self):
        pass

    @abc.abstractmethod
    def query(self, sql_query: SqlQuery) -> List[str]:
        """
        :param str sql_query: the sql query to perform
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def table_exists(self, table_name: str) -> bool:
        """returns true if the given table exists in the database
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def create_table(self, table_name: str, fields: List[SqlTableField]):
        """creates the table in this sql database
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def delete_table(self, table_name: str):
        """deletes the given table in this sql database
        """
        raise NotImplementedError()

    # @abc.abstractmethod
    # def add_table_row(self, table_name: str, values: List[SqlTableField]):
    #     """creates the table in this sql database
    #     """
    #     raise NotImplementedError()

    @abc.abstractmethod
    def get_field_directive(self, field_name: str, field_sql_type: str, field_description: str) -> str:
        """returns the sql directive for the declaration of the given table field (eg "`matrix_size` real NOT NULL COMMENT 'the size of the matrix'")
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def dump(self, sql_file_path: Path):
        """dumps this database into the given sql file"""
        raise NotImplementedError()


class RemoteMysqlDb(ISqlDatabaseBackend):
    def __init__(self, db_server_fqdn, db_user, db_name):
        """
        :param str db_server_fqdn: the fully qualified domain name of the server hosting the database, eg simpatix10.univ-rennes1.fr
        :param str db_user: the user for accessing the inventory database, eg simpadb_reader
        :param str db_name: the name of the inventory database, eg simpadb
        """
        self._db_server_fqdn = db_server_fqdn
        self._db_user = db_user
        self._db_name = db_name
        self._connect()

    def _connect(self):
        self._conn = MySQLdb.connect(self._db_server_fqdn, self._db_user, '', self._db_name)
        assert self._conn

    def query(self, sql_query) -> List[str]:
        """
        :param str sql_query: the sql query to perform
        """
        self._conn.query(sql_query)
        rows = self._conn.store_result()
        return rows

    def table_exists(self, table_name: str) -> bool:
        rows = self.query(f"SHOW TABLES LIKE '{table_name}';")
        assert len(rows) <= 1, f'Unexpected case: more than one ({len(rows)}) tables match the table name {table_name}.'
        return len(rows) == 1

    def create_table(self, table_name: str, fields: List[SqlTableField]):
        raise NotImplementedError()

    def get_field_directive(self, field_name: str, field_sql_type: str, field_description: str) -> str:
        return f'`{field_name}` {field_sql_type} COMMENT \'{field_description}\''

    def dump(self, sql_file_path: Path):
        raise NotImplementedError()


class SshAccessedMysqlDb(ISqlDatabaseBackend):

    """a mysql database server accessed using ssh instead of a remote mysql client

    Instead of accessing the remote sql database from mysql client, this method ssh connects (we expect an unattended connection setup using ssh keys) to the server hosting the database, and once logged in performs a request to the database locally using mysql client running on the server hosting the database.

    This method has a the following benefits over the simple use of mysql:
    - it's more secure since it benefits from ssh encryption
    - it doesn't require extra open tcp ports for remote mysql database access

    """
    def __init__(self, db_server_fqdn: str, db_user: str, db_name: str, ssh_user: str):
        """
        :param str db_server_fqdn: the fully qualified domain name of the server hosting the database, eg iprbenchdb.ipr.univ-rennes1.fr
        :param str db_user: the user for accessing the inventory database, eg iprbenchw
        :param str db_name: the name of the database, eg iprbench
        :param str ssh_user: the user on db_server_fqdn that has access to the database db_name
        """
        self._db_server_fqdn = db_server_fqdn
        self._db_user = db_user
        self._db_name = db_name
        self.ssh_user = ssh_user

    def query(self, sql_query: SqlQuery) -> List[str]:
        """
        :param str sql_query: the sql query to perform
        """
        escaped_sql_command = sql_query.replace('`', r'\\\`').replace('"', r'\\\"')
        command = f'ssh "{self.ssh_user}@{self._db_server_fqdn}" "echo \\"use {self._db_name}; {escaped_sql_command}\\" | mysql --defaults-group-suffix={self._db_user}"'
        completed_process = subprocess.run(command, shell=True, check=False, capture_output=True)
        if completed_process.returncode != 0:
            logging.error(completed_process.stderr.decode(encoding='utf-8'))
            assert False
        rows = completed_process.stdout.decode('utf-8').split('\n')
        return rows

    def table_exists(self, table_name: str) -> bool:
        rows = self.query(f"SHOW TABLES LIKE '{table_name}';")
        logging.debug('len(rows) = %d', len(rows))
        logging.debug('rows = %s', str(rows))
        assert len(rows) <= 3, f'Unexpected case: more than one ({len(rows) - 2}) tables match the table name {table_name}.'
        if len(rows) >= 3:
            assert re.match(r'^Tables_in_', rows[0]), f'unexpected value for the 1st line : {rows[0]}. (the 1st line is expected to contain something like "Tables_in_test_iprbenchs (dummy)")'
            assert rows[1] == table_name
        return len(rows) == 3

    def create_table(self, table_name: str, fields: List[SqlTableField]):
        # https://www.sqlite.org/autoinc.html
        # > The AUTOINCREMENT keyword imposes extra CPU, memory, disk space, and disk I/O overhead and should be avoided if not strictly needed. It is usually not needed.
        fields_sql_descriptions = []
        for field in fields:
            sql_field_type = {
                SqlTableField.Type.FIELD_TYPE_FLOAT: 'real NOT NULL',
                SqlTableField.Type.FIELD_TYPE_INT: 'int(11) NOT NULL',
                SqlTableField.Type.FIELD_TYPE_STRING: 'varchar(256) NOT NULL',
                SqlTableField.Type.FIELD_TYPE_TIME: 'datetime NOT NULL',
            }[field.field_type]
            if field.is_autoinc_index:
                assert field.field_type == SqlTableField.Type.FIELD_TYPE_INT
                sql_field_type = 'INTEGER PRIMARY KEY AUTO_INCREMENT'
            fields_sql_description = self.get_field_directive(field.name, sql_field_type, field.description)
            fields_sql_descriptions.append(fields_sql_description)

        sql_create_table_command = f'CREATE TABLE `{table_name}` ({",".join(fields_sql_descriptions)});'
        logging.debug('sql_create_table_command = %s', sql_create_table_command)
        self.query(sql_create_table_command)

    def delete_table(self, table_name: str):
        sql_create_table_command = f'DROP TABLE `{table_name}`;'
        self.query(sql_create_table_command)

    def get_field_directive(self, field_name: str, field_sql_type: str, field_description: str) -> str:
        return f'`{field_name}` {field_sql_type} COMMENT \'{field_description}\''

    def dump(self, sql_file_path: Path):
        # mysqldump -u root --quote-names --opt --single-transaction --quick $db >
        command = f'ssh "{self.ssh_user}@{self._db_server_fqdn}" "mysqldump --defaults-group-suffix={self._db_user}" {self._db_name} > {sql_file_path}'
        _ = subprocess.run(command, shell=True, check=False, capture_output=True)


class SqliteDb(ISqlDatabaseBackend):
    sqlite_db_path: Union[Path, str]  # ':memory:'  # sqlite-specific special name for a file stored in memory. We could use something like '/tmp/simpadb.sqlite' here but this would make parsing really slow (1 minute instead of 1s), unless either :
    _con: sqlite3.Connection
    _cur: sqlite3.Cursor

    def __init__(self, sqlite_db_path: Path):
        """
        :param str sqlite_db_path: the path of the sqlite database
        :param str database_name: the name of the database withing the sqlite database (eg "iprbench")
        """
        self.sqlite_db_path = sqlite_db_path
        self._cur = None  

        check_same_thread = False
        # this is to prevent the following error when run from apache/django : SQLite objects created in a thread can only be used in that same thread.The object was created in thread id 139672342353664 and this is thread id 139672333960960
        # accordig to https://stackoverflow.com/questions/48218065/programmingerror-sqlite-objects-created-in-a-thread-can-only-be-used-in-that-sa this is ok, as long as there are no concurrent writes
        # If set False, the returned connection may be shared across multiple threads. When using multiple threads with the same connection writing operations should be serialized by the user to avoid data corruption
        # I hope it's safe here but I'm not 100% sure though. Anyway, if the database gets corrupt, it not a big deal since this memory resident database gets reconstructed from the sql file...

        if sqlite_db_path == ':memory:' or not sqlite_db_path.exists():
            logging.debug('creating sqlite database in %s', sqlite_db_path)
            self._con = sqlite3.connect(sqlite_db_path, check_same_thread=check_same_thread)
        else:
            logging.debug('reusing existing sqlite database in %s', sqlite_db_path)
            self._con = sqlite3.connect(sqlite_db_path, check_same_thread=check_same_thread)
        self._cur = self._con.cursor()
        logging.debug('self._con = %s', self._con)
        logging.debug('self._cur = %s', self._cur)

        _ = self.query('PRAGMA encoding="UTF-8";')

    def query(self, sql_query) -> List[str]:
        """
        :param str sql_query: the sql query to perform
        """
        self._cur.execute(sql_query)
        rows = self._cur.fetchall()
        self._con.commit()
        return rows

    def table_exists(self, table_name: str) -> bool:
        rows = self.query(f"SELECT name FROM sqlite_master WHERE type='table' AND name='{table_name}';")
        assert len(rows) <= 1, f'Unexpected case: more than one ({len(rows)}) tables match the table name {table_name}.'
        return len(rows) == 1

    def create_table(self, table_name: str, fields: List[SqlTableField]):
        # https://www.sqlite.org/autoinc.html
        # > The AUTOINCREMENT keyword imposes extra CPU, memory, disk space, and disk I/O overhead and should be avoided if not strictly needed. It is usually not needed.
        fields_sql_descriptions = []
        for field in fields:
            sql_field_type = {
                SqlTableField.Type.FIELD_TYPE_FLOAT: 'real NOT NULL',
                SqlTableField.Type.FIELD_TYPE_INT: 'int(11) NOT NULL',
                SqlTableField.Type.FIELD_TYPE_STRING: 'varchar(256) NOT NULL',
                SqlTableField.Type.FIELD_TYPE_TIME: 'datetime NOT NULL',
            }[field.field_type]
            if field.is_autoinc_index:
                assert field.field_type == SqlTableField.Type.FIELD_TYPE_INT
                sql_field_type = 'INTEGER PRIMARY KEY'
            fields_sql_description = self.get_field_directive(field.name, sql_field_type, field.description)
            fields_sql_descriptions.append(fields_sql_description)

        sql_create_table_command = f'CREATE TABLE `{table_name}` ({",".join(fields_sql_descriptions)});'
        logging.debug('sql_create_table_command = %s', sql_create_table_command)
        self.query(sql_create_table_command)

    def delete_table(self, table_name: str):
        sql_create_table_command = f'DROP TABLE `{table_name}`;'
        self.query(sql_create_table_command)

    def get_field_directive(self, field_name: str, field_sql_type: str, field_description: str) -> str:
        # sqlite doesn't understand the COMMENT keyword, so we use sql comments ( "--" ), as explained in [https://stackoverflow.com/questions/7426205/sqlite-adding-comment-on-descriptions-to-tables-and-columns]
        return f'`{field_name}` {field_sql_type} -- {field_description}\n'

    def dump(self, sql_file_path: Path):
        with open(sql_file_path, 'wt', encoding='utf8') as f:
            for line in self._con.iterdump():
                f.write(line)


class SqlFile(SqliteDb):
    _sql_file_path: Path

    def __init__(self, sql_file_path: Path, truncate_hex_strings=False):
        """
        :param str sql_file_path: the path of the sql file containing the inventory database
        """
        self._sql_file_path = sql_file_path
        self._cur = None  # sqlite cursor

        sqlite_db_path = ':memory:'  # sqlite-specific special name for a file stored in memory. We could use something like '/tmp/simpadb.sqlite' here but this would make parsing really slow (1 minute instead of 1s), unless either :
        # - proper fix : group of INSERT statements are surrounded by BEGIN and COMMIT (see http://stackoverflow.com/questions/4719836/python-and-sqlite3-adding-thousands-of-rows)
        # - the file is stored on a solid state disk
        try:
            os.remove(sqlite_db_path)
        except BaseException:
            pass
        super().__init__(sqlite_db_path)
        with open(str(self._sql_file_path), 'r', encoding='utf8') as f:  # str conversion has been added to support older versions of python in which open don't accept arguments of type Path
            sql = f.read()  # watch out for built-in `str`
        # print(sql)
        self._cur = self._con.cursor()
        # print(mysql_to_sqlite(sql))
        sqlite_sql = mysql_to_sqlite(sql, truncate_hex_strings)
        # with open('/tmp/toto.sqlite.sql', 'w') as f:
        #    f.write(sqlite_sql)
        # with open('/tmp/toto.sqlite.sql', 'r') as f:
        #    sqlite_sql = f.read()
        self._cur.executescript(sqlite_sql)


class TableAttrNotFound(Exception):
    def __init__(self, table, key_name, key_value, attr_name):
        message = "failed to find in table %s a value for %s where %s is %s" % (table, attr_name, key_name, key_value)
        super(TableAttrNotFound, self).__init__(message)
        self.table = table
        self.key_name = key_name
        self.key_value = key_value
        self.attr_name = attr_name


class SqlDatabaseReader(object):

    def __init__(self, inv_provider: ISqlDatabaseBackend):
        """
        :param ISqlDatabaseBackend inv_provider: the input that provides the inventory data
        """
        self._inv_provider = inv_provider

    def query(self, sql_query: SqlQuery):
        """
        performs a query on the sql database

        :param SqlQuery sql_query: the sql query to perform
        """
        return self._inv_provider.query(sql_query)

    def get_table_attr(self, table, key_name, key_value, attr_name):
        """
        reads the value of the fiven attribute of the given item in the given table

        :param str table: the name of the table to read
        :param str key_name: the name of the column that stores the id of the item to read
        :param str key_value: the id of the item to read
        :param str attr_name: the name of the attribute to read from the item
        """
        attr_value = None
        rows = self.query("SELECT " + attr_name + " FROM " + table + " WHERE " + key_name + "='" + key_value + "'")
        if len(rows) > 0:
            attr_value = rows[0][0]
        else:
            raise TableAttrNotFound(table, key_name, key_value, attr_name)
        return attr_value


def machineNameToMacAddress(machineName):
    conn = MySQLdb.connect('simpatix10', 'simpadb_reader', '', 'simpadb')
    assert conn
    sqlQuery = """SELECT mac_address FROM ethernet_cards WHERE machine_name='""" + machineName + """' AND type='normal'"""
    # print sqlQuery
    conn.query(sqlQuery)
    r = conn.store_result()
    row = r.fetch_row(0)
    assert len(row) == 1
    # print 'row =', row
    macAddress = row[0][0]
    # print macAddress
    conn.close()
    return macAddress


def getLightOutManagementIpAddress(machineName):
    """
        the light out management ip of servers allows to talk to the server even when it's asleep
    """
    conn = MySQLdb.connect('simpatix10', 'simpadb_reader', '', 'simpadb')
    assert conn
    sqlQuery = """SELECT ip_address_1,ip_address_2,ip_address_3,ip_address_4 FROM ethernet_cards WHERE machine_name='""" + machineName + """' AND type='light_out_management'"""
    # print sqlQuery
    conn.query(sqlQuery)
    r = conn.store_result()
    row = r.fetch_row(0)
    assert len(row) == 1
    # print 'row =', row
    ipAddress = ('%s.%s.%s.%s') % (row[0][0], row[0][1], row[0][2], row[0][3])
    # print macAddress
    conn.close()
    return ipAddress


def get_cluster_machines_names():
    clusterMachinesNames = []
    conn = MySQLdb.connect('simpatix10', 'simpadb_reader', '', 'simpadb')
    assert conn
    sqlQuery = """SELECT name FROM machines WHERE affectation='cluster'"""
    # print sqlQuery
    conn.query(sqlQuery)
    r = conn.store_result()
    rows = r.fetch_row(0)
    for row in rows:
        # print row
        clusterMachinesNames.append(row[0])
    conn.close()
    return clusterMachinesNames


def machineSupportsIpmi(machineName):
    if (machineName == 'simpatix') or (machineName == 'simpatix01' or (machineName == 'simpa-mac2')):
        # the command ipmitool sensor on simpatix doesn't work :
        # Unabled to establish a session with the BMC.
        # Command failed due to Unknown (0xFFFEF921) (0xFFFEF921)
        return False
    return True


def putToSleep(machineName):
    # note : pmset must be executed as root
    (returnCode, stdout, _stderr) = execute_command(['ssh', machineName, 'pmset sleepnow'])
    # print returnCode
    # print 'stdout :'
    # print stdout
    # print 'stderr :'
    # print stderr
    assert returnCode == 0
    # check if the command succeeded by looking at the output (that's the only way I found)
    f = StringIO.StringIO(stdout)
    line = f.readline()
    f.close()
    matchObj = re.match('^Sleeping now...', line)
    if matchObj:
        return True
    else:
        return False


def wakeUp(machineName):
    macAddress = machineNameToMacAddress(machineName)
    wake_on_lan(macAddress)
    return True


def isNonRespondingMachineSleeping(machineName):
    """
        note : crappy method to detect if the machine is sleeping (if other methods are available, I would be very interested)
    """
    wakeUp(machineName)
    time.sleep(120)
    if is_machine_responding(machineName):
        putToSleep(machineName)
        time.sleep(30)  # allow a little time to make sure the machine is ready to receive other wake on lan messages
        return True
    else:
        return False


if __name__ == '__main__':
    # for i in range(30):
    #     machineName = 'simpatix%d' % (i+10)
    #     print 'lom ip of %s is %s' % (machineName, getLightOutManagementIpAddress(machineName))
    wakeUp('simpatix21')
    # print putToSleep('simpatix13')
    # print isNonRespondingMachineSleeping('simpatix13')
