class Version(object):
    """
    simple version number made of a series of positive integers separated by dots

    distutils.version.StrictVersion : not good because versions such as 3.2.0.4 are not allowed (StrictVersion allows no more than 3 numbers)
    distutils.version.LooseVersion : not good because the version string could be anything (https://stackoverflow.com/questions/11887762/how-do-i-compare-version-numbers-in-python)

    """
    def __init__(self, version_as_string):
        """
        :param str version_as_string: eg '6.2u5' or '8.1.9'
        """
        self.numbers = [int(s) for s in version_as_string.replace('u', '.').split('.')]

    def get_number(self, index):
        if index >= len(self.numbers):
            return 0
        else:
            return self.numbers[index]

    def __eq__(self, other):
        for i in range(max(len(self.numbers), len(other.numbers))):
            if self.get_number(i) != other.get_number(i):
                return False
        return True

    def __lt__(self, other):
        return other.__gt__(self)

    def __gt__(self, other):
        for i in range(max(len(self.numbers), len(other.numbers))):
            if self.get_number(i) < other.get_number(i):
                return False
            if self.get_number(i) > other.get_number(i):
                return True
            # if numbers ar equal continue to next version number
        return False  # numbers are equal

    def __ge__(self, other):
        return self.__gt__(other) or self.__eq__(other)

    def __le__(self, other):
        return self.__lt__(other) or self.__eq__(other)

    def __str__(self):
        return '.'.join([str(n) for n in self.numbers])
