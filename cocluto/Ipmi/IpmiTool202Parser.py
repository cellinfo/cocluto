import io
import re
from Sensor import FanSensor, TemperatureSensor
from ClusterNodeSensorsReadings import ClusterNodeSensorsReadings


class IpmiTool202Parser:
    def parseSensorOutput(self, strOutput, clusterNodeName):
        sensorReadings = ClusterNodeSensorsReadings(clusterNodeName)
        f = io.StringIO(strOutput)
        line = f.readline()
        while len(line) > 0:
            # print line,
            matchObj = re.match(r'^Sensor ID[ ]*\: \'(?P<sensorName>[a-zA-Z 0-9]+)\'', line)
            if matchObj:
                sensorName = matchObj.group('sensorName')
                # print sensorName
                # read the entity id
                line = f.readline()
                matchObj = re.match(r'^ Entity ID[ ]*\: (?P<entityId>[0-9\.]+)', line)
                assert matchObj
                entityId = matchObj.group('entityId')
                # print entityId
                # read the sensor type
                line = f.readline()
                matchObj = re.match(r'^ Sensor Type[\(\)a-zA-Z ]*\: (?P<sensorType>[a-zA-Z \(\)]+)', line)
                assert matchObj
                sensorType = matchObj.group('sensorType')
                # print sensorType
                if sensorType == 'Fan':
                    rpms = self.parseFanSensorOutput(f)
                    if temperature is not None:
                        sensor = FanSensor(sensorName)
                        sensor.rpms = rpms
                elif sensorType == 'Temperature':
                    temperature = self.parseTemperatureSensorOutput(f)
                    if temperature is not None:
                        sensor = TemperatureSensor(sensorName)
                        sensor.temperature = temperature
                else:
                    # ignoring other sensors
                    sensor = None
                if sensor:
                    sensorReadings.addSensor(sensor)
            else:
                None
                # assert(False)
            line = f.readline()
        f.close()

    def parseFanSensorOutput(self, file):
        """
            reads the fan specific ipdmitool output
        """
        line = file.readline()
        # print line
        matchObj = re.match(r'^ Sensor Reading[ ]*\: (?P<numRpms>[0-9]+) \(\+/\- (?P<rpmsPrecision>[0-9]+)\) RPM', line)
        if matchObj:
            numRpms = matchObj.group('numRpms')
            # print numRpms
            rpms = float(numRpms)
            return rpms
        else:
            matchObj = re.match(r'^ Sensor Reading[ ]*\: Not Present', line)
            assert matchObj
            return None

    def parseTemperatureSensorOutput(self, file):
        """
            reads the temperature specific ipdmitool output
        """
        # Sensor Reading        : 36 (+/- 0) degrees C
        line = file.readline()
        # print line
        matchObj = re.match(r'^ Sensor Reading[ ]*\: (?P<temperature>[0-9]+) \(\+/\- (?P<precision>[0-9]+)\) degrees C', line)
        if matchObj:
            temperature = matchObj.group('temperature')
            temperature = float(temperature)
            return temperature
        else:
            matchObj = re.match(r'^ Sensor Reading[ ]*\: Not Present', line)
            assert matchObj
            return None
