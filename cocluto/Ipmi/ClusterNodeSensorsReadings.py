import Sensor


class ClusterNodeSensorsReadings:
    """

    """
    """
    POWERSTATE_UNKNOWN=0
    POWERSTATE_OFF=1
    POWERSTATE_ON=2
    POWERSTATE_SLEEP=3
    """
    def __init__(self, clusterNodeName):
        self.cluster_node_name = clusterNodeName
        self.sensors = {}
        # self.power_state = ClusterNodeStatus.POWERSTATE_UNKNOWN
        return

    def addSensor(self, sensor):
        self.sensors[sensor.name] = sensor

    def dump(self):
        for key, sensor in self.sensors.items():
            sensor.dump()
        return

    # def get_power_state(self):
    #    return self.power_state

    def getLowestTemperature(self):
        # log('ClusterNodeSensorsReadings::getLowestTemperature : start')
        lowestTemperature = 0.0
        lowestTemperatureIsDefined = False
        for key, sensor in self.sensors.items():
            # log('ClusterNodeSensorsReadings::getLowestTemperature : start')
            if sensor.typeName() == 'Temperature':
                sensor.temperature
                if lowestTemperatureIsDefined:
                    if sensor.temperature < lowestTemperature:
                        lowestTemperature = sensor.temperature
                else:
                    lowestTemperature = sensor.temperature
                    lowestTemperatureIsDefined = True
        assert lowestTemperatureIsDefined
        # log('ClusterNodeSensorsReadings::getLowestTemperature : end')
        return lowestTemperature
