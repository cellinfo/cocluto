from typing import Optional


class Sensor:

    def __init__(self, sensorName):
        self.name = sensorName
        self.is_valid = True  # false if this sensor is not actually present on the target machine
        return

    def dump(self):
        print(self.name)


class FanSensor(Sensor):
    rpms: Optional[float]

    def __init__(self, sensorName):
        Sensor.__init__(self, sensorName)
        self.rpms = None

    def dump(self):
        print('Fan \'', self.name, '\' rpm=', self.rpms)

    def typeName(self):
        return 'Fan'


class TemperatureSensor(Sensor):
    temperature: Optional[float]

    def __init__(self, sensorName):
        Sensor.__init__(self, sensorName)
        self.temperature = None

    def dump(self):
        print('Temperature \'', self.name, '\' temperature=', self.temperature)

    def typeName(self):
        return 'Temperature'
