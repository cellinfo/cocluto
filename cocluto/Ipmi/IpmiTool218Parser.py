import io
import re
from Sensor import FanSensor, TemperatureSensor
from ClusterNodeSensorsReadings import ClusterNodeSensorsReadings


class IpmiTool218Parser:
    def parseSensorOutput(self, strOutput, clusterNodeName):
        sensorReadings = ClusterNodeSensorsReadings(clusterNodeName)
        f = io.StringIO(strOutput)
        line = f.readline()
        while len(line) > 0:
            # print line,
            matchObj = re.match(r'^(?P<sensorName>[a-zA-Z 0-9]+[a-zA-Z 0-9]*[a-zA-Z0-9])[ ]*\| (?P<sensorValue>[\.0-9]+)[ ]*\| (?P<sensorUnit>[a-zA-Z0-9][a-zA-Z 0-9]*[a-zA-Z0-9])[?]*', line)
            if matchObj:
                # log('readClusterNodeSensorsIpmiTool2_1_8 : sensorName = '+matchObj.group('sensorName'))
                # log('readClusterNodeSensorsIpmiTool2_1_8 : sensorValue = '+matchObj.group('sensorValue'))
                # log('readClusterNodeSensorsIpmiTool2_1_8 : sensorUnit = "'+matchObj.group('sensorUnit')+'"')
                sensorName = matchObj.group('sensorName')
                sensorValue = matchObj.group('sensorValue')
                sensorUnit = matchObj.group('sensorUnit')
                sensor = None
                if sensorUnit == 'degrees C':
                    sensor = TemperatureSensor(sensorName)
                    sensor.temperature = float(sensorValue)
                elif sensorUnit == 'RPM':
                    sensor = FanSensor(sensorName)
                    sensor.rpms = float(sensorValue)
                else:
                    None
                if sensor:
                    # log('readClusterNodeSensorsIpmiTool2_1_8 : adding sensor')
                    sensorReadings.addSensor(sensor)
            else:
                None
                # assert(False)
            line = f.readline()
        f.close()
        return sensorReadings
