﻿#!/usr/bin/python

# import sys
# sys.path.insert(0, '/homes/raffy/SvnGRaffy/dev/Python')
import re
# import Lib.Util


class SgeConfig:

    def __init__(self):
        self.m_attrs = {}

    def hasAttr(self, attr_name):
        return attr_name in self.m_attrs.keys()

    def getAttr(self, strAttrName):
        return self.m_attrs[strAttrName]

    def setAttr(self, strAttrName, strAttrValue):
        assert isinstance(strAttrName, str)
        assert isinstance(strAttrValue, str)
        self.m_attrs[strAttrName] = strAttrValue

    def loadFromSgeFormat1String(self, strSgeConfigString):
        """
            loads attrs from a string such as :
                hostname              simpatix11.univ-rennes1.fr
                load_scaling          NONE
                complex_values        has_molpro_2010=0
                load_values           arch=darwin-x86,num_proc=4,mem_total=8192.000000M, \
                                      swap_total=0.000000M,virtual_total=8192.000000M, \
                                      load_avg=5.126465,load_short=5.186523, \
                                      load_medium=5.126465,load_long=5.087891, \
                                      mem_free=6654.054688M,swap_free=0.000000M, \
                                      virtual_free=6654.054688M,mem_used=1537.945312M, \
                                      swap_used=0.000000M,virtual_used=1537.945312M, \
                                      cpu=100.000000,m_topology=NONE,m_topology_inuse=NONE, \
                                      m_socket=0,m_core=0,np_load_avg=1.281616, \
                                      np_load_short=1.296631,np_load_medium=1.281616, \
                                      np_load_long=1.271973
                processors            4
                user_lists            NONE
                xuser_lists           NONE
                projects              NONE
                xprojects             NONE
                usage_scaling         NONE
                report_variables      NONE
        """
        self.m_attrs = {}
        # put multiline attributes on one line
        strSgeConfigString = re.sub(r"\\\n", "", strSgeConfigString)
        for strAttrDef in strSgeConfigString.split("\n"):
            # print("strAttrDef=%s" % strAttrDef)
            if len(strAttrDef) != 0:
                matchObj = re.match(r"^(?P<attrName>[^\s]+)[ ]+(?P<attrValue>[^\s].*)$", strAttrDef)
                assert matchObj is not None
                # print('%s = %s\n' % (matchObj.group("attrName"), matchObj.group("attrValue")))
                self.m_attrs[matchObj.group("attrName")] = matchObj.group("attrValue")

    def loadFromSgeFormat2String(self, strSgeConfigString):
        """
            loads attrs from a string such as :
                arch=darwin-x86,num_proc=4,mem_total=8192.000000M, \
                      swap_total=0.000000M,virtual_total=8192.000000M, \
                      load_avg=5.126465,load_short=5.186523, \
                      load_medium=5.126465,load_long=5.087891, \
                      mem_free=6654.054688M,swap_free=0.000000M, \
                      virtual_free=6654.054688M,mem_used=1537.945312M, \
                      swap_used=0.000000M,virtual_used=1537.945312M, \
                      cpu=100.000000,m_topology=NONE,m_topology_inuse=NONE, \
                      m_socket=0,m_core=0,np_load_avg=1.281616, \
                      np_load_short=1.296631,np_load_medium=1.281616, \
                      np_load_long=1.271973
        """
        self.m_attrs = {}
        if strSgeConfigString != "NONE":
            for strAttrDef in strSgeConfigString.split(","):
                # print strAttrDef
                if len(strAttrDef) != 0:
                    matchObj = re.match(r"^\s*(?P<attrName>[^=]+)=(?P<attrValue>.*)$", strAttrDef)
                    # print matchObj.group("attrName")
                    self.m_attrs[matchObj.group("attrName")] = matchObj.group("attrValue")

    def asFormat1String(self):
        strResult = ""
        for (k, v) in self.m_attrs.items():
            # print "%s %s" % (k,v)
            # if the attribute's value is a list of comma separated strings, make sure there are no spaces after the commas, otherwise the value is not properly interpreted when read back into sge
            # for example if the user sets the value of administrator_mail (using qconf -mconf global) to "alice@univ-rennes1.fr,    bob@univ-rennes1.fr", then the next call to qconf -sconf global will show a wrong value for administrator_mail, as shown below:
            # pag_cmd                      none
            # administrator_mail           alice@univ-rennes1.fr,
            # token_extend_time            none

            # it's even worse, as it messes with the whole config, putting unwanted attributes in the reporting_params attribute. In short, inputting commas followed by spaces seems to confuse sge....

            # the tests below show that administrator_mail can only take a value, which can be a separator separated list, in which a separator is either :
            # - separator_form_a: a comma character (no spaces after)
            # - separator_form_b: a comma character, followed by any number of spaces, then a backslash, then \n

            # because we remove carriage returns in our values, the only storage option is separator_form_a

            # administrator_mail           alice@univ-rennes1.fr
            # -> ok

            # administrator_mail           alice@univ-rennes1.fr,bob@univ-rennes1.fr
            # -> ok

            # administrator_mail           alice@univ-rennes1.fr, bob@univ-rennes1.fr
            # -> messes up

            # administrator_mail           alice@univ-rennes1.fr, \
            # bob@univ-rennes1.fr
            # -> ok

            # administrator_mail           alice@univ-rennes1.fr,bob@univ-rennes1.fr, \
            # bob2@univ-rennes1.fr
            # -> ok

            # administrator_mail           alice@univ-rennes1.fr,bob@univ-rennes1.fr,  \
            # bob2@univ-rennes1.fr
            # -> ok

            # administrator_mail           alice@univ-rennes1.fr,bob@univ-rennes1.fr \
            # bob2@univ-rennes1.fr
            # -> error
            #     root@physix-master:~# qconf -Mconf /tmp/global
            #     only a single value is allowed for configuration attribute "administrator_mail"

            cleaned_value = re.sub(r',\s*', ',', v)

            # prevent space pollution in space separated values, such as in reporting_params (see https://bugzilla.ipr.univ-rennes1.fr/show_bug.cgi?id=2812). If spaces are not compacted, the space separated values will contain more and more spaces and at some point corrupt the value : a line containing just a backslash, such as in the following example:
            # reporting_params             accounting=true reporting=false                   \
            #                              flush_time=00:00:15 joblog=false                  \
            #                              sharelog=00:00:00
            #  \
            cleaned_value = re.sub(r'\s+', ' ', cleaned_value)
            strResult += "%s %s\n" % (k, cleaned_value)
        # print("strResult=%s" % strResult)
        return strResult

    def asFormat2String(self):
        strResult = ""
        iNumAttrs = len(self.m_attrs)
        if iNumAttrs == 0:
            return "NONE"
        iAttr = 0
        for (k, v) in self.m_attrs.items():
            # print "%s %s" % (k,v)
            strResult += "%s=%s" % (k, v)
            if iAttr != (iNumAttrs - 1):
                strResult += ","
            iAttr += 1
        # print strSgeConfigString
        return strResult

    def dump(self):
        for (k, v) in self.m_attrs.items():
            print("['%s']='%s'" % (k, v))
