from typing import Optional
from .Job import QueueMachineId
from .ClusterNode import ClusterNodeId


class QueueMachineStateFlags:  #
    DISABLED = 1    # the queue machine is disabled
    ALARM = 2    # the queue machine is in alarm state (see man qstat)
    UNKNOWN = 4  # the queue machine is in unknown state because sge_execd cannot be contected (see man qstat)
    ERROR = 8  # the queue is in error state
    OBSOLETE = 16  # the queue no longer exists but it is still visible because it still contains running jobs
    SUSPENDED = 32  # the queue machine is suspended


class QueueMachine:
    """
        a QueueMachine instance represents a given SGE queue on a given machine (eg allintel.q@simpatix10)
    """
    queue_name: str
    machine_name: ClusterNodeId
    num_slots: Optional[int]
    num_used_slots: Optional[int]
    cpu_load: Optional[float]
    state_flags: int
    disable_message: str

    def __init__(self, queueName, machineName):
        self.queue_name = queueName
        self.machine_name = machineName
        self.num_slots = None
        self.num_used_slots = None
        self.cpu_load = None
        self.state_flags = 0
        self.disable_message = ''

    def get_name(self) -> QueueMachineId:
        """
            returns the name of the machine queue (such as allintel.q@simpatix10)
        """
        return self.queue_name + '@' + self.machine_name

    def get_queue_name(self) -> str:
        return self.queue_name

    def get_machine_name(self) -> str:
        return self.machine_name

    def set_num_slots(self, num_slots: int):
        self.num_slots = num_slots

    def set_num_used_slots(self, num_slots: int):
        self.num_used_slots = num_slots

    def get_num_slots(self) -> int:
        assert self.num_slots is not None
        return self.num_slots

    def get_num_used_slots(self) -> int:
        assert self.num_used_slots is not None
        return self.num_used_slots

    def set_cpu_load(self, cpu_load: float):
        self.cpu_load = cpu_load

    def cpu_load_is_available(self) -> bool:
        return self.cpu_load is not None

    def get_cpu_load(self) -> float:
        assert self.cpu_load is not None
        return self.cpu_load

    def set_state(self, state: int):
        self.state_flags = state

    def is_disabled(self) -> bool:
        return self.state_flags & QueueMachineStateFlags.DISABLED

    def is_in_error_state(self) -> bool:
        return self.state_flags & QueueMachineStateFlags.ERROR

    def is_responding(self) -> bool:
        return not (self.state_flags & QueueMachineStateFlags.UNKNOWN)

    def is_in_alarm_state(self) -> bool:
        return self.state_flags & QueueMachineStateFlags.ALARM

    def is_suspended(self) -> bool:
        return self.state_flags & QueueMachineStateFlags.SUSPENDED
