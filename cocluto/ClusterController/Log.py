import time
import threading

gLogFilePath = '/tmp/ClusterController.log'  # '/var/log/ClusterController.log'


def log(message):
    threadName = threading.currentThread().get_name()
    logMessage = time.asctime(time.localtime()) + ' : ' + threadName + ' : ' + message
    print(logMessage)
    f = open(gLogFilePath, 'a+', encoding='utf8')
    assert f
    try:
        f.write(logMessage + '\n')
    finally:
        f.close()


def logDebug(message):
    log('[D]' + message)
    return


def log_info(message):
    log('[I]' + message)


def log_warning(message):
    log('[W]' + message)


def logError(message):
    log('[E]' + message)
