
import abc
import time
import copy
from .PowerState import PowerState
from .Log import log_info


class Slot:
    def __init__(self):
        self.queue_machine = None
        self.num_slots = None
        self.jobs = None                # job for which this slot is allocated


class SlotAllocator(abc.ABCMeta):
    """
        a class that defines a strategy for allocating free slots for the given pending jobs
    """
    @abc.abstractmethod
    def get_machinesThatNeedWakeUp(self, pendingJobs, clusterState):
        """
            returns the list of machines that need to wake up to make pending jobs running
        """
        assert False  # this method is abstract


class SimpleSlotAllocator(SlotAllocator):

    def get_machinesThatNeedWakeUp(self, pendingJobs, clusterState):
        machinesThatNeedWakeUp = {}
        highestPriorityPendingJob = pendingJobs.values()[0]
        log_info('SimpleSlotAllocator::get_machinesThatNeedWakeUp : looking for free slots for job ' + highestPriorityPendingJob.getId().asStr())
        numFreeSlots = {}  # contains the number of free slots for each queueMachine
        for queueMachine in clusterState.get_jobs_state().get_queue_machines().values():
            numFreeSlots[queueMachine] = clusterState.get_jobs_state().getNumFreeSlotsOnQueueMachine(queueMachine)
            log_info('SimpleSlotAllocator::get_machinesThatNeedWakeUp : init numFreeSlots[%s] with %d ' % (queueMachine.get_name(), numFreeSlots[queueMachine]))
        remainingNumSlotsToAllocate = highestPriorityPendingJob.job_requirements.num_slots
        log_info('SimpleSlotAllocator::get_machinesThatNeedWakeUp : still %d slots to find' % remainingNumSlotsToAllocate)
        # first look in running machines if there are available slots
        for queueMachine in clusterState.get_jobs_state().get_queue_machines().values():
            log_info('SimpleSlotAllocator::get_machinesThatNeedWakeUp : examining queueMachine %s ' % queueMachine.get_name())
            machine = clusterState.get_machines()[queueMachine.get_machine_name()]
            if machine.get_power_state() == PowerState.ON:
                if clusterState.queue_machine_fits_job_requirements(queueMachine, highestPriorityPendingJob.job_requirements):
                    numSlotsAllocatedOnThisMachine = min(numFreeSlots[queueMachine], remainingNumSlotsToAllocate)
                    log_info('SimpleSlotAllocator::get_machinesThatNeedWakeUp : found %d slots on already running %s ' % (numSlotsAllocatedOnThisMachine, queueMachine.get_machine_name()))

                    remainingNumSlotsToAllocate -= numSlotsAllocatedOnThisMachine
                    numFreeSlots[queueMachine] -= numSlotsAllocatedOnThisMachine
                    log_info('SimpleSlotAllocator::get_machinesThatNeedWakeUp : still %d slots to find' % remainingNumSlotsToAllocate)
                    assert remainingNumSlotsToAllocate >= 0
                    if remainingNumSlotsToAllocate == 0:
                        break
        if remainingNumSlotsToAllocate > 0:
            # now look into machines that are asleep
            for queueMachine in clusterState.get_jobs_state().get_queue_machines().values():
                log_info('SimpleSlotAllocator::get_machinesThatNeedWakeUp : examining queueMachine %s ' % queueMachine.get_name())
                machine = clusterState.get_machines()[queueMachine.get_machine_name()]
                if machine.get_power_state() == PowerState.SLEEP:
                    if clusterState.queue_machine_fits_job_requirements(queueMachine, highestPriorityPendingJob.job_requirements):
                        numSlotsAllocatedOnThisMachine = min(numFreeSlots[queueMachine], remainingNumSlotsToAllocate)
                        log_info('SimpleSlotAllocator::get_machinesThatNeedWakeUp : found %d slots on sleeping %s ' % (numSlotsAllocatedOnThisMachine, queueMachine.get_machine_name()))
                        remainingNumSlotsToAllocate -= numSlotsAllocatedOnThisMachine
                        numFreeSlots[queueMachine] -= numSlotsAllocatedOnThisMachine
                        machinesThatNeedWakeUp[machine.get_name()] = machine
                        log_info('SimpleSlotAllocator::get_machinesThatNeedWakeUp : still %d slots to find' % remainingNumSlotsToAllocate)
                        assert remainingNumSlotsToAllocate >= 0
                        if remainingNumSlotsToAllocate == 0:
                            break
        if remainingNumSlotsToAllocate != 0:
            return {}  # not enough slots available
        return machinesThatNeedWakeUp


class DecoupledSlotAllocator(SlotAllocator):
    """
        a slot allocator that doesn't know much about sge, and does not attempts to guess what sge'sceduler would do
        Instead, it uses a very simple strategy : it wakes up all the machines periodically to allow jobs to get in.
    """
    def __init__(self):
        self.delay_between_periodic_checks = -1  # in seconds. Disable periodic checks by setting this to -1
        self.last_check_time = time.time()
        self.last_cluster_state = None

    def jobsStateHasChanged(self, newClusterState):
        """
            returns true if there is a change in the cluster state that can cause a pending job
            to start (provided all machines are enabled)
        """
        oldJobs = {}
        if self.last_cluster_state:
            oldJobs = self.last_cluster_state.jobs_state.jobs
        newJobs = newClusterState.jobs_state.jobs
        bJobsHaveChanged = False
        oldJobsOnly = oldJobs.copy()  # shallow copy
        # print 'oldJobs : ', oldJobs
        # print 'newJobs : ', newJobs
        # print 'self.last_cluster_state', self.last_cluster_state
        # print 'newClusterState', newClusterState
        # if self.last_cluster_state:
        #     print 'self.last_cluster_state.jobs_state', self.last_cluster_state.jobs_state
        # print 'newClusterState.jobs_state', newClusterState.jobs_state
        # print 'id(self.last_cluster_state) : ', id(self.last_cluster_state)
        # print 'id(newClusterState) : ', id(newClusterState)
        # print 'len(oldJobs) : ', len(oldJobs)
        # print 'len(newJobs) : ', len(newJobs)
        # print 'id(oldJobs) : ', id(oldJobs)
        # print 'id(newJobs) : ', id(newJobs)
        for newJob in newJobs.values():
            # logDebug('DecoupledSlotAllocator::jobsStateHasChanged newJob id=%s' % newJob.getId().asStr())
            if newJob.getId() in oldJobs:
                # logDebug('DecoupledSlotAllocator::jobsStateHasChanged job id=%d is in old jobs' % newJob.getId())
                del oldJobsOnly[newJob.getId()]
            else:
                # ah ... a new job has arrived
                log_info('A new job (jobId =%s) has been detected ' % newJob.getId().asStr())
                bJobsHaveChanged = True
        if len(oldJobsOnly) != 0:
            for oldJob in oldJobsOnly.values():
                log_info('Job (jobId =%s) has finished' % oldJob.getId().asStr())
            # at least one old job has finished, freeing some slots
            bJobsHaveChanged = True
        return bJobsHaveChanged

    def get_machinesThatNeedWakeUp(self, pendingJobs, clusterState):
        machinesThatNeedWakeUp = {}
        bJobsStateHasChanged = self.jobsStateHasChanged(clusterState)  # pylint: disable=no-value-for-parameter
        currentTime = time.time()
        # we do periodic checks to detect changes in cluster state that are not detected by jobsStateHasChanged
        # for example changes in the requirements, in the allocation policy, etc...
        bItsTimeForPeriodicCheck = False
        if self.delay_between_periodic_checks > 0:
            bItsTimeForPeriodicCheck = (currentTime - self.last_check_time) > self.delay_between_periodic_checks
        if bJobsStateHasChanged or bItsTimeForPeriodicCheck:
            if bJobsStateHasChanged:
                log_info('DecoupledSlotAllocator::get_machinesThatNeedWakeUp : waking up machines that are asleep because jobs state has changed')
            else:
                log_info('DecoupledSlotAllocator::get_machinesThatNeedWakeUp : waking up machines that are asleep for periodic check (to be sure pending jobs get a chance to start)')
            for queueMachine in clusterState.get_jobs_state().get_queue_machines().values():
                if queueMachine.get_machine_name() in clusterState.get_machines():
                    # this means that the machine is under the cluster controller's control
                    machine = clusterState.get_machines()[queueMachine.get_machine_name()]
                    if machine.get_power_state() == PowerState.SLEEP:
                        machinesThatNeedWakeUp[machine.get_name()] = machine
            self.last_check_time = currentTime
        self.last_cluster_state = copy.copy(clusterState)
        # print 'self.last_cluster_state', self.last_cluster_state
        return machinesThatNeedWakeUp
