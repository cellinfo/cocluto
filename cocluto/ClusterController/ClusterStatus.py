import time
from typing import Dict, Optional, List
import threading
from .Job import TaskUid, Task, QueueMachineId, JobRequirements
from .JobsStateUpdater import JobsStateUpdater
from .ClusterNode import ClusterNode, ClusterNodeId
from .Log import log_info, logError
from .PowerState import PowerState
from ..SimpaDbUtil import get_cluster_machines_names
from .SunGridEngine import SunGridEngine
from .JobsState import JobsState


class ClusterStatus:
    """
        The current state (jobs, sensors) of the cluster

        @param    gridEngine    the interface to the batch job tool (in our case it's sun grid engine)
    """
    grid_engine: SunGridEngine
    cluster_nodes: Dict[ClusterNodeId, ClusterNode]
    lock: threading.Lock  # to prevent concurrent access to this instance
    jobs_state_updater: JobsStateUpdater
    jobs_state: Optional[JobsState]
    controlled_machine_names: List[ClusterNodeId]

    def __init__(self, grid_engine: SunGridEngine):
        self.grid_engine = grid_engine
        self.cluster_nodes = {}
        self.lock = threading.Lock()
        self.jobs_state_updater = JobsStateUpdater(self)
        self.jobs_state = None
        # self.controlled_machine_names = ['simpatix30']
        self.controlled_machine_names = []  # ['simpatix30']
        if False:
            for iMachine in range(11, 40):
                if (iMachine == 31) or (iMachine == 32):
                    continue  # these machines don't seem to be able to go to sleep properly (bug 00000010)
                if (iMachine == 18):
                    continue  # this machine needs maintenance (restarting because it's very slow for an unknown reason)
                self.controlled_machine_names.append('simpatix%d' % iMachine)
        node_names = get_cluster_machines_names()
        for node_name in node_names:
            if node_name in self.controlled_machine_names:
                log_info('machine %s is under the cluster controller\'s control' % node_name)
                cluster_node = ClusterNode(node_name, self, grid_engine)
                if node_name == 'simpatix10':
                    cluster_node.set_should_always_be_on()
                self.cluster_nodes[node_name] = cluster_node
        return

    def set_control_on_machine(self, machine_name: ClusterNodeId, control: bool):
        if control:
            # add machineName under control of ClusterController
            for _k, v in self.cluster_nodes.items():
                if v.get_name() == machine_name:
                    return  # nothing to do : machineName is already under the control of ClusterController

            cluster_node = ClusterNode(machine_name, self, self.grid_engine)
            if machine_name == 'simpatix10':
                cluster_node.set_should_always_be_on()
            self.cluster_nodes[machine_name] = cluster_node
            cluster_node.machine_status_updater.start()
        else:
            # remove machineName from control of ClusterController
            cluster_node = self.cluster_nodes.get(machine_name)
            if cluster_node:
                cluster_node.machine_status_updater.stop = True
                cluster_node.machine_status_updater.join()
                self.cluster_nodes.pop(machine_name)

    def get_grid_engine(self) -> SunGridEngine:
        return self.grid_engine

    def get_machines(self) -> Dict[ClusterNodeId, ClusterNode]:
        return self.cluster_nodes

    def start_reading_threads(self):
        for _k, v in self.cluster_nodes.items():
            v.machine_status_updater.start()
        self.jobs_state_updater.start()

    def stop_reading_threads(self):
        for _k, v in self.cluster_nodes.items():
            v.machine_status_updater.stop = True
            v.machine_status_updater.join()
        self.jobs_state_updater.stop = True
        self.jobs_state_updater.join()

    def on_new_jobs_state(self, new_jobs_state: JobsState):
        # logDebug('ClusterStatus::on_new_jobs_state : attempting to acquire lock to access jobs_state')
        self.lock.acquire()
        # logDebug('ClusterStatus::on_new_jobs_state : got lock to access jobs_state')
        self.jobs_state = new_jobs_state
        self.lock.release()

    def get_jobs_on_machine(self, machine_name: ClusterNodeId) -> Dict[TaskUid, Task]:
        return self.jobs_state.get_jobs_on_machine(machine_name)

    def is_ready(self) -> bool:
        for _k, v in self.cluster_nodes.items():
            if not v.is_ready():
                log_info('ClusterStatus::is_ready : not ready because of ' + v.get_name())
                return False
            # log('ClusterStatus::is_ready() : '+k+' is ready')
            # assert(False)
        if self.jobs_state is None:
            log_info('ClusterStatus::is_ready : not ready because waiting for jobs state')
            return False
        return True

    def get_idle_machines(self) -> Dict[ClusterNodeId, ClusterNode]:
        assert self.is_ready
        bBUG_00000009_IS_STILL_ALIVE = True
        if bBUG_00000009_IS_STILL_ALIVE:
            currentTime = time.time()
            fJOBS_STATE_MAX_ALLOWED_AGE = 3600
            fJobsStateAge = currentTime - self.jobs_state.get_time()
            if fJobsStateAge > fJOBS_STATE_MAX_ALLOWED_AGE:
                logError('ClusterStatus::get_idle_machines : age of jobs state is too old (%f s). This is bug 00000009.' % (fJobsStateAge))
                assert False
        idleMachines = {}
        for machineName, machine in self.cluster_nodes.items():
            if machine.get_power_state() == PowerState.ON:
                jobsOnThisMachine = self.get_jobs_on_machine(machineName)
                if len(jobsOnThisMachine) == 0:
                    idleMachines[machineName] = machine
        return idleMachines

    def get_pending_jobs(self) -> Dict[TaskUid, Task]:
        return self.jobs_state.get_pending_jobs()

    def get_jobs_state(self) -> JobsState:
        return self.jobs_state

    def queue_machine_fits_job_requirements(self, queue_machine: QueueMachineId, job_requirements: JobRequirements) -> bool:
        if job_requirements.queues:
            bQueueIsInAllowedQueues = False
            for queueName in job_requirements.queues:
                if queueName == queue_machine.get_queue_name():
                    bQueueIsInAllowedQueues = True
            if not bQueueIsInAllowedQueues:
                log_info('queue_machine_fits_job_requirements : queue_machine ' + queue_machine.get_name() + ' rejected because it\'s not in the allowed queues')
                return False
        return True

    def get_energy_consumption(self) -> float:
        """
            returns an estimate of the energy consumption since the start of the cluster controller (in joules)
        """
        fEnergyConsumption = 0.0
        for machine in self.cluster_nodes.values():
            if machine.is_ready():  # there are cases where the machine is not ready yet (for example, it's just been added to clustercontroller's control)
                fEnergyConsumption += machine.get_energy_consumption()
        return fEnergyConsumption

    def get_energy_savings(self) -> float:
        """
            returns an estimate of the energy saving since the start of the cluster controller (in joules)
        """
        fEnergySavings = 0.0
        for machine in self.cluster_nodes.values():
            if machine.is_ready():
                fEnergySavings += machine.get_energy_savings()
        return fEnergySavings

    def get_current_power_consumption(self) -> float:
        power_consumption = 0.0
        for machine in self.cluster_nodes.values():
            if machine.is_ready():
                power_consumption += machine.get_power_consumption()
        return power_consumption

    def get_current_power_savings(self) -> float:
        power_savings = 0.0
        for machine in self.cluster_nodes.values():
            if machine.is_ready():
                power_savings += machine.get_power_consumption_for_power_state(PowerState.ON) - machine.get_power_consumption()
        return power_savings

    def get_num_controlled_slots(self) -> int:
        self.lock.acquire()
        num_controlled_slots = 0
        for machine in self.cluster_nodes.values():
            queue_machine = self.jobs_state.get_queue_machine(machine.get_name())
            num_controlled_slots += queue_machine.get_num_slots()
        self.lock.release()
        return num_controlled_slots

    def get_num_used_slots(self) -> int:
        self.lock.acquire()
        num_used_slots = 0
        for machine in self.cluster_nodes.values():
            queue_machine = self.jobs_state.get_queue_machine(machine.get_name())
            num_used_slots_on_this_machine = queue_machine.get_num_slots() - self.jobs_state.getNumFreeSlotsOnQueueMachine(queue_machine)
            assert num_used_slots_on_this_machine >= 0
            num_used_slots += num_used_slots_on_this_machine
        self.lock.release()
        return num_used_slots

    def get_num_wasted_slots(self) -> int:
        self.lock.acquire()
        iNumWastedSlots = 0
        for machine in self.cluster_nodes.values():
            if machine.get_power_state() == PowerState.ON:
                queue_machine = self.jobs_state.get_queue_machine(machine.get_name())
                iNumWastedSlots += self.jobs_state.getNumFreeSlotsOnQueueMachine(queue_machine)
        self.lock.release()
        return iNumWastedSlots

    def get_num_sleeping_slots(self) -> int:
        self.lock.acquire()
        iNumSleepingSlots = 0
        for machine in self.cluster_nodes.values():
            if machine.get_power_state() == PowerState.SLEEP:
                queue_machine = self.jobs_state.get_queue_machine(machine.get_name())
                iNumSleepingSlots += self.jobs_state.getNumFreeSlotsOnQueueMachine(queue_machine)
        self.lock.release()
        return iNumSleepingSlots
