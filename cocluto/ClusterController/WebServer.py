# Copyright Jon Berg , turtlemeat.com
import cgi
import time
from os import curdir, sep
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer  # pylint:disable=import-error
import threading
# import pri
from urlparse import urlparse, parse_qs  # pylint:disable=import-error
import xml.dom.minidom
from .Util import on_exception
# >>> url = 'http://example.com/?foo=bar&one=1'
# >>> parse_qs(urlparse(url).query)
# {'foo': ['bar'], 'one': ['1']}


class MyHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        try:
            paramsDict = parse_qs(urlparse(self.path).query)
            if self.path.endswith(".html"):
                f = open(curdir + sep + self.path, encoding='utf8')  # self.path has /test.html
                # note that this potentially makes every file on your computer readable by the internet

                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(f.read())
                f.close()
                return
            if self.path.endswith(".esp"):  # our dynamic content
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write("hey, today is the" + str(time.localtime()[7]))
                self.wfile.write(" day in the year " + str(time.localtime()[0]))
                return
            if self.path.endswith("ShowControlledMachines"):  # http://simpatix10.univ-rennes1.fr:8080/ShowControlledMachines
                self.send_response(200)
                self.send_header('Content-type', 'text/xml')
                self.end_headers()

                # Create the minidom document
                doc = xml.dom.minidom.Document()

                # Create the <ControlledMachines> root element
                controlledMachinesElement = doc.createElement("ControlledMachines")
                doc.appendChild(controlledMachinesElement)

                for machine in self.server.cluster_controller.cluster_status.cluster_nodes.values():
                    # Create the main <card> element
                    controlledMachineElement = doc.createElement("Machine")
                    controlledMachineElement.setAttribute("name", machine.get_name())
                    controlledMachinesElement.appendChild(controlledMachineElement)
                # Print our newly created XML
                self.wfile.write(doc.toprettyxml(indent="  "))
                return
            if urlparse(self.path).path == '/SetControlOnMachine':  # http://simpatix10.univ-rennes1.fr:8080/SetControlOnMachine?machineName=simpatix30&control=1
                machineName = paramsDict['machineName'][0]
                bControl = (paramsDict['control'][0] == '1')
                self.server.cluster_controller.set_control_on_machine(machineName, bControl)
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                if bControl is True:
                    self.wfile.write("%s is now controlled by ClusterController" % machineName)
                else:
                    self.wfile.write("%s is no longer controlled by ClusterController" % machineName)
                return
            return
        except IOError:
            self.send_error(404, 'File Not Found: %s' % self.path)

    def do_POST(self):
        try:
            ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
            if ctype == 'multipart/form-data':
                query = cgi.parse_multipart(self.rfile, pdict)
            self.send_response(301)

            self.end_headers()
            upfilecontent = query.get('upfile')
            print("filecontent", upfilecontent[0])
            self.wfile.write("<HTML>POST OK.<BR><BR>")
            self.wfile.write(upfilecontent[0])

        except BaseException:
            pass


class WebServerThread(threading.Thread):
    stop: bool
    http_server: HTTPServer

    def __init__(self, clusterController):
        threading.Thread.__init__(self)
        # self.cluster_controller = clusterController
        self.stop = False
        self.http_server = HTTPServer(('', 8080), MyHandler)
        self.http_server.cluster_controller = clusterController

    def run(self):
        try:
            while not self.stop:
                self.http_server.handle_request()
            # self.http_server.serve_forever()
        except BaseException as exception:  # catches all exceptions, including the ctrl+C (KeyboardInterrupt)
            self.http_server.socket.close()
            on_exception(exception)
