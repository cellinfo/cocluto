#!/usr/bin/env python

"""
    script that installs ClusterController on simpatix10
    to start ClusterController :
        launchctl start fr.univ-rennes1.ipr.ClusterController    
"""
import sys
sys.path.insert(0, '..')
from Lib.Util import *
import os

if __name__ == '__main__':
    machineName = 'simpatix10'
    strThisDir = os.getcwd()
    strPythonDevDir = strThisDir + '/..'
    print( 'installing ClusterController on '+machineName )
    remoteCommand = ''
    remoteCommand += 'mkdir -p /usr/local/bin/ipr/Python;'
    remoteCommand += 'rm -r /usr/local/bin/ipr/Python/Lib;'
    remoteCommand += 'rm -r /usr/local/bin/ipr/Python/ClusterController;'
    remoteCommand += 'cp -r %s/Lib /usr/local/bin/ipr/Python/;' % strPythonDevDir
    remoteCommand += 'cp -r %s/ClusterController /usr/local/bin/ipr/Python/;' % strPythonDevDir
    remoteCommand += 'cp %s/ClusterController/ClusterController.plist /Library/LaunchDaemons/fr.univ-rennes1.ipr.ClusterController.plist;' % strPythonDevDir
    remoteCommand += 'cp -r %s/ClusterController/ClusterControllerLauncher.sh /usr/local/bin/ipr/Python/ClusterController/;' % strPythonDevDir
    remoteCommand += 'launchctl unload /Library/LaunchDaemons/fr.univ-rennes1.ipr.ClusterController.plist;'
    remoteCommand += 'launchctl load /Library/LaunchDaemons/fr.univ-rennes1.ipr.ClusterController.plist;'
    command = 'ssh root@'+ machineName +' "'+remoteCommand+'"'
    ( returnCode, stdout, stderr ) = execute_command( command )
    for strSingleCommand in remoteCommand.split(';'):
        print(strSingleCommand)
    print(stdout)
    print(stderr)
    if returnCode == 0:
        print('install succeeded on '+machineName)
    else:
        print('install failed on '+machineName+' (see below for detail)')
        print stderr
        #assert( False )
        
     
            
