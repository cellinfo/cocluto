from typing import TYPE_CHECKING
from typing import Optional, List
import threading
import time
import abc
from .PowerState import PowerState
from .Log import log_info, logDebug
from .Util import blocking_wake_up_machine, blocking_put_machine_to_sleep, get_power_state, on_exception
if TYPE_CHECKING:
    from .ClusterNode import ClusterNodeId, ClusterNode
    from .SunGridEngine import SunGridEngine


class IWakeUpCompleteNotifier(abc.ABCMeta):
    """
        interface for wakeup notifiers
    """
    @abc.abstractmethod
    def on_wake_up_complete(self):
        assert False


class ISleepCompleteNotifier(abc.ABCMeta):
    """
        interface for sleep notifiers
    """
    @abc.abstractmethod
    def on_sleep_complete(self, bSleepSucceeded):
        assert False


class IRequest(abc.ABCMeta):
    GO_TO_SLEEP = 1
    WAKE_UP = 2
    CHECK_POWER_STATE = 3

    def __init__(self, requestType):
        self.type = requestType

    def getType(self):
        return self.type

    @abc.abstractmethod
    def process(self, clusterNodeStatusUpdater):
        """
            processes this request
        """
        assert False  # this method is abstract


class WakeUpRequest(IRequest):

    def __init__(self, wakeUpNotifier: IWakeUpCompleteNotifier):
        IRequest.__init__(self, IRequest.WAKE_UP)
        self.wake_up_notifier = wakeUpNotifier

    def process(self, clusterNodeStatusUpdater):
        assert clusterNodeStatusUpdater.should_always_be_on is False  # are we attempting to wake up a machine that should always be on ?
        log_info('Handling wakeup request for %s' % clusterNodeStatusUpdater.get_name())
        bSuccess = blocking_wake_up_machine(clusterNodeStatusUpdater.get_name())
        assert bSuccess
        # activate the associated machine queue
        if clusterNodeStatusUpdater.set_queue_activation(True):
            pass  # all is ok
        else:
            assert False
        clusterNodeStatusUpdater.state_lock.acquire()
        clusterNodeStatusUpdater.cluster_node.set_power_state(PowerState.ON)
        clusterNodeStatusUpdater.state_lock.release()
        if self.wake_up_notifier:
            logDebug('ClusterNodeStatusUpdater::run : Sending wakeup notification')
            self.wake_up_notifier.on_wake_up_complete()


class SleepRequest(IRequest):

    def __init__(self, sleepCompleteNotifier: ISleepCompleteNotifier):
        IRequest.__init__(self, IRequest.GO_TO_SLEEP)
        self.sleep_complete_notifier = sleepCompleteNotifier

    def process(self, clusterNodeStatusUpdater):
        assert not clusterNodeStatusUpdater.should_always_be_on  # are we attempting to put a machine the should stay on to sleep ?
        log_info('Handling sleep request for %s' % clusterNodeStatusUpdater.get_name())
        if clusterNodeStatusUpdater.set_queue_activation(False):
            if clusterNodeStatusUpdater.queue_is_empty():
                if blocking_put_machine_to_sleep(clusterNodeStatusUpdater.cluster_node_name):
                    # now we know that the machine is asleep
                    clusterNodeStatusUpdater.state_lock.acquire()
                    clusterNodeStatusUpdater.cluster_node.set_power_state(PowerState.SLEEP)
                    clusterNodeStatusUpdater.state_lock.release()
                    if self.sleep_complete_notifier:
                        self.sleep_complete_notifier.on_sleep_complete(True)
                else:
                    assert False
            else:
                # reactivate the queue
                if not clusterNodeStatusUpdater.set_queue_activation(True):
                    assert False
                clusterNodeStatusUpdater.state_lock.acquire()
                clusterNodeStatusUpdater.cluster_node.set_power_state(PowerState.ON)  # this is necessary to reenable the various cyclic checks that were disabled on sleep request
                clusterNodeStatusUpdater.state_lock.release()
                clusterNodeStatusUpdater.cluster_node.on_sleep_because_a_job_just_arrived()
                if self.sleep_complete_notifier:
                    self.sleep_complete_notifier.on_sleep_complete(False)
        else:
            assert False


class CheckPowerStateRequest(IRequest):

    def __init__(self):
        IRequest.__init__(self, IRequest.CHECK_POWER_STATE)

    def process(self, clusterNodeStatusUpdater):
        powerState = get_power_state(clusterNodeStatusUpdater.cluster_node_name)
        clusterNodeStatusUpdater.state_lock.acquire()
        clusterNodeStatusUpdater.cluster_node.on_new_power_state_reading(powerState)
        clusterNodeStatusUpdater.last_power_check_state_time = time.time()
        clusterNodeStatusUpdater.state_lock.release()


class ClusterNodeStatusUpdater(threading.Thread):
    cluster_node_name: 'ClusterNodeId'
    cluster_node: 'ClusterNode'
    grid_engine: 'SunGridEngine'
    stop: bool
    last_power_check_state_time: Optional[time.time]
    check_power_state: bool
    check_sensors: Optional[bool]
    state_lock: threading.Lock  # lock that prevents concurrent access to the state of this instance
    should_always_be_on: bool  # indicates that the machine should never go to sleep or off for whatever reason (eg simpatix10)
    pending_requests_queue: List[IRequest]
    DELAY_BETWEEN_POWERSTATE_CHECKS = 5 * 60  # in seconds

    def __init__(self, machineName: 'ClusterNodeId', clusterNode: 'ClusterNode', gridEngine: 'SunGridEngine'):
        threading.Thread.__init__(self)
        self.cluster_node_name = machineName
        self.cluster_node = clusterNode
        self.grid_engine = gridEngine
        self.stop = False
        self.last_power_check_state_time = None
        self.check_power_state = True
        self.state_lock = threading.Lock()
        self.should_always_be_on = False
        self.pending_requests_queue = []
        self.check_sensors = None

    def get_grid_engine(self):
        return self.grid_engine

    def get_name(self):
        return self.cluster_node_name

    def set_should_always_be_on(self):
        print('%s should always be on' % (self.get_name()))
        self.should_always_be_on = True

    def push_request(self, request: IRequest):
        self.state_lock.acquire()
        self.pending_requests_queue.append(request)
        self.state_lock.release()

    def pop_request(self) -> IRequest:
        oldest_request = None
        self.state_lock.acquire()
        if len(self.pending_requests_queue) != 0:
            oldest_request = self.pending_requests_queue.pop(0)
        self.state_lock.release()
        return oldest_request

    def run(self):
        try:

            while not self.stop:
                # handle the oldest request
                request = self.pop_request()
                if request is not None:
                    request.process(self)

                # schedule a power state check if required
                currentTime = time.time()
                if self.check_power_state:
                    if not self.should_always_be_on:  # don't do power checks on such machines because some current implementations of
                        # operations involved might cause the machine to go to sleep
                        if (not self.last_power_check_state_time) or (currentTime > (self.last_power_check_state_time + ClusterNodeStatusUpdater.DELAY_BETWEEN_POWERSTATE_CHECKS)):
                            self.push_request(CheckPowerStateRequest())  # pylint: disable=no-value-for-parameter

                time.sleep(1)
        except BaseException as exception:  # catches all exceptions, including the ctrl+C (KeyboardInterrupt)
            on_exception(exception)

    def request_sleep(self, sleep_complete_notifier: Optional[ISleepCompleteNotifier] = None):
        assert not self.should_always_be_on
        self.push_request(SleepRequest(sleep_complete_notifier))  # pylint: disable=no-value-for-parameter

    def request_wake_up(self, wake_up_complete_notifier: Optional[IWakeUpCompleteNotifier] = None):
        assert self.should_always_be_on is False
        self.push_request(WakeUpRequest(wake_up_complete_notifier))  # pylint: disable=no-value-for-parameter

    def get_queue_machine_name(self):
        return self.cluster_node.get_queue_machine_name()

    def set_queue_activation(self, bEnable: bool):
        """
            @return    true on success, false otherwise
        """
        return self.get_grid_engine().set_queue_instance_activation(self.get_queue_machine_name(), bEnable)

    def queue_is_empty(self):
        return self.get_grid_engine().queue_is_empty(self.get_name())
