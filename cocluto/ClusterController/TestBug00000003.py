#!/usr/bin/env python
from .Log import log_info
from .Util import get_power_state, blocking_put_machine_to_sleep, blocking_wake_up_machine, execute_command, execute_ipmi_command
from .PowerState import PowerState


def Test0000():
    log_info('Testing bug 00000003 if a series of wake up, goto sleep can shutdown a machine')
    strTarget_machine_name = 'simpatix12'
    ePowerState = get_power_state(strTarget_machine_name)
    while True:
        if ePowerState == PowerState.ON:
            bSuccess = blocking_put_machine_to_sleep(strTarget_machine_name)
            assert bSuccess
            bSuccess = blocking_put_machine_to_sleep(strTarget_machine_name)
            ePowerState = PowerState.SLEEP
        elif ePowerState == PowerState.SLEEP:
            bSuccess = blocking_wake_up_machine(strTarget_machine_name)
            assert bSuccess
            ePowerState = PowerState.ON
        else:
            assert False


def Test0001():
    log_info('Testing bug 00000003 : could it be caused by a sleep and a power on at the same tim ?')
    strTarget_machine_name = 'simpatix12'
    ePowerState = get_power_state(strTarget_machine_name)
    if ePowerState == PowerState.SLEEP:
        bSuccess = blocking_wake_up_machine(strTarget_machine_name)
        assert bSuccess
        ePowerState = PowerState.ON
    assert ePowerState == PowerState.ON
    execute_command("ssh %s 'pmset sleepnow'" % strTarget_machine_name)
    bSuccess = blocking_wake_up_machine(strTarget_machine_name)
    assert bSuccess


def Test0002():
    log_info('Testing bug 00000003 : could it be caused by a power on quickly followed by a sleep ?')
    strTarget_machine_name = 'simpatix12'
    ePowerState = get_power_state(strTarget_machine_name)
    if ePowerState == PowerState.ON:
        bSuccess = blocking_wake_up_machine(strTarget_machine_name)
        assert bSuccess
        ePowerState = PowerState.SLEEP
    assert ePowerState == PowerState.SLEEP
    execute_ipmi_command(strTarget_machine_name, 'chassis power on')
    execute_command("ssh %s 'pmset sleepnow'" % strTarget_machine_name)


if __name__ == '__main__':
    Test0000()
