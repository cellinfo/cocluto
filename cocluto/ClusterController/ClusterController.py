#!/usr/bin/env python
from typing import Dict, Optional
import sys
import os
import MySQLdb
import threading
import time
from datetime import datetime
if sys.version_info < (3, 0):
    from HTMLParser import HTMLParser
else:
    from html.parser import HTMLParser
from ..Util import log
# from ..SimpaDbUtil import toto
from .ClusterNode import ClusterNodeId, ClusterNode
from .ClusterStatus import ClusterStatus
from .SlotAllocator import DecoupledSlotAllocator, SlotAllocator
from .Log import logDebug, log_info
from .ClusterNodeStatusUpdater import IWakeUpCompleteNotifier, ISleepCompleteNotifier
from .SunGridEngine import SunGridEngine
from .Util import on_exception
from .WebServer import WebServerThread
from .PowerState import PowerState

VERSION = '1.18'


class MyHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.token_list = []

    def handle_data(self, data):
        data = data.strip()
        if data and len(data) > 0:
            self.token_list.append(data)
            # print data

    def get_token_list(self):
        return self.token_list


class WakeUpCompleteNotifier(IWakeUpCompleteNotifier):

    def __init__(self, machineName, clusterController):
        self.machine_name = machineName
        self.cluster_controller = clusterController

    def on_wake_up_complete(self):
        logDebug('WakeUpCompleteNotifier::on_wake_up_complete : start')
        self.cluster_controller.onMachineWakeUpComplete(self.machine_name)


class SleepCompleteNotifier(ISleepCompleteNotifier):

    def __init__(self, machineName, clusterController):
        self.machine_name = machineName
        self.cluster_controller = clusterController

    def on_sleep_complete(self, bSleepSucceeded):
        logDebug('SleepCompleteNotifier::on_sleep_complete : start')
        self.cluster_controller.onMachineSleepComplete(self.machine_name, bSleepSucceeded)


def jouleToKwh(fEnergyInJoules):
    """
        converts joules to kWH
    """
    # 1 kWh = 1000 * 3600 J
    return fEnergyInJoules / (1000.0 * 3600.0)


class ClusterController:
    """
        The cluster controller monitors the cluster's activity and has multiple purposes :
            - energy saving : it can put some machines to sleep if they have nothing to do, or it
            can wake them up when needed (eg when a new job has arrived)
            - auto-repair : for examples
                - it happened sometimes that sge_execd process disappeared for some unknown reason
                in that case, the cluster controller can detect it and restart the daemon
                automatically, without administrator's intervention
                - clear the Error state of queues
            - it could also be used to dynamically adapt sge's settings to the requirements of
            jobs (eg add some machines to a queue).
        Mechanism to let user get priority
    """
    cluster_status: ClusterStatus
    slot_allocator = SlotAllocator
    machines_that_need_wake_up: Dict[ClusterNodeId, ClusterNode]
    machines_that_need_wake_up_lock: threading.Lock  # to prevent concurrent access to machines_that_need_wake_up
    machines_that_need_sleeping: Dict[ClusterNodeId, ClusterNode]
    machines_that_need_sleeping_lock: threading.Lock  # to prevent concurrent access to machines_that_need_sleeping
    last_energy_status_log_time: Optional[datetime]
    DELAY_BETWEEN_ENERGY_STATUS_LOGS: int  # in seconds
    session_id: Optional[int]  # session (run) identifier in database
    web_server: WebServerThread
    stop: bool
    stop_lock: threading.Lock  # to prevent concurrent access to stop

    def __init__(self):
        gridEngine = SunGridEngine()
        self.cluster_status = ClusterStatus(gridEngine)
        self.slot_allocator = DecoupledSlotAllocator()  # SimpleSlotAllocator()  pylint: disable=no-value-for-parameter
        self.machines_that_need_wake_up = {}
        self.machines_that_need_wake_up_lock = threading.Lock()
        self.machines_that_need_sleeping = {}
        self.machines_that_need_sleeping_lock = threading.Lock()
        self.last_energy_status_log_time = None
        self.DELAY_BETWEEN_ENERGY_STATUS_LOGS = 60
        self.session_id = None
        self.web_server = WebServerThread(self)
        self.stop = False
        self.stop_lock = threading.Lock()

    def getClusterStatus(self):
        return self.cluster_status

    def log(self, message):
        print(message)

    def shutdownLeastImportantNode(self):
        self.log("ClusterController::shutdownLeastImportantNode : start")

    def onMachineWakeUpComplete(self, machineName):
        self.machines_that_need_wake_up_lock.acquire()
        # logDebug('ClusterController::onMachineWakeUpComplete : machine %s old len(self.machines_that_need_wake_up) = %d' % (machineName,len(self.machines_that_need_wake_up)))
        del self.machines_that_need_wake_up[machineName]
        # logDebug('ClusterController::onMachineWakeUpComplete : machine %s new len(self.machines_that_need_wake_up) = %d' % (machineName,len(self.machines_that_need_wake_up)))
        self.machines_that_need_wake_up_lock.release()
        logDebug('ClusterController::onMachineWakeUpComplete : removed %s from the list of machines that need waking up because it\'s now awake' % machineName)

    def onMachineSleepComplete(self, machineName, bSleepSucceeded):
        self.machines_that_need_sleeping_lock.acquire()
        # logDebug('ClusterController::onMachineSleepComplete : machine %s old len(self.machines_that_need_wake_up) = %d' % (machineName,len(self.machines_that_need_wake_up)))
        del self.machines_that_need_sleeping[machineName]
        # logDebug('ClusterController::onMachineSleepComplete : machine %s new len(self.machines_that_need_wake_up) = %d' % (machineName,len(self.machines_that_need_wake_up)))
        self.machines_that_need_sleeping_lock.release()
        if bSleepSucceeded:
            logDebug('ClusterController::onMachineSleepComplete : removed %s from the list of machines that need sleeping because it\'s now sleeping' % machineName)
        else:
            logDebug('ClusterController::onMachineSleepComplete : removed %s from the list of machines that need sleeping because it can\'t be put to sleep at the moment (eg a job just arrived)' % machineName)

    def getNumPendingWakeUps(self):
        self.machines_that_need_wake_up_lock.acquire()
        numPendingWakeUps = len(self.machines_that_need_wake_up)
        self.machines_that_need_wake_up_lock.release()
        return numPendingWakeUps

    def getNumPendingSleeps(self):
        self.machines_that_need_sleeping_lock.acquire()
        numPendingSleeps = len(self.machines_that_need_sleeping)
        self.machines_that_need_sleeping_lock.release()
        return numPendingSleeps

    def putIdleMachinesToSleep(self):
        self.cluster_status.lock.acquire()
        idleMachines = self.cluster_status.get_idle_machines()
        # log_info('idleMachines :')
        for _machineName, idleMachine in idleMachines.items():
            if idleMachine.get_power_state() == PowerState.ON:
                # log_info('\t%s' % machineName)
                if idleMachine.get_name() != 'simpatix10':  # never put simpatix10 to sleep because it's the sge master and is also server for other things
                    self.machines_that_need_sleeping[idleMachine.get_name()] = idleMachine
        self.cluster_status.lock.release()

        listOfMachinesThatNeedSleeping = self.machines_that_need_sleeping.values()  # duplicate the list so that we don't iterate on machines_that_need_sleeping, which could cause a runtime error because callbacks alter machines_that_need_wake_up
        for machine in listOfMachinesThatNeedSleeping:
            log_info('ClusterController::putIdleMachinesToSleep : requesting sleep for %s because it\'s idle' % machine.get_name())
            machine.request_sleep(SleepCompleteNotifier(machine.get_name(), self))  # pylint: disable=no-value-for-parameter

        if len(listOfMachinesThatNeedSleeping) != 0:
            # hack : wait until the sleep requests are handled so that we don't request the same machine to sleep multiple times
            while self.getNumPendingSleeps() > 0:
                time.sleep(1)

    def wake_up_machinesForPendingJobs(self):
        listOfMachinesThatNeedWakeUp = []

        self.cluster_status.lock.acquire()
        pendingJobs = self.cluster_status.get_pending_jobs()
        # log_info('pending jobs :')
        # for job in     pendingJobs.values():
        #     log_info('\t%d' % job.getId().asStr())
        if len(pendingJobs) != 0:
            self.machines_that_need_wake_up = self.slot_allocator.get_machinesThatNeedWakeUp(pendingJobs, self.cluster_status)
            if len(self.machines_that_need_wake_up) == 0:
                pass
                # log_info('ClusterController::updateNormalState : no machine needs waking up')
            else:
                listOfMachinesThatNeedWakeUp = self.machines_that_need_wake_up.values()  # duplicate the list so that we don't iterate on machines_that_need_wake_up, which would cause a runtime error because callbacks alter machines_that_need_wake_up
        for machine in listOfMachinesThatNeedWakeUp:
            log_info('ClusterController::wake_up_machinesForPendingJobs : requesting wake up for ' + machine.get_name())
            machine.request_wake_up(WakeUpCompleteNotifier(machine.get_name(), self))  # pylint: disable=no-value-for-parameter
        self.cluster_status.lock.release()

        if len(listOfMachinesThatNeedWakeUp) != 0:
            # hack : wait until the wakeup requests are handled so that a later sleep request doesn't cancel it
            # and also wait for the jobs to come in
            while self.getNumPendingWakeUps() > 0:
                time.sleep(1)
            iSGE_CHEK_RUNNABLE_JOBS_DELAY = 60 * 5  # max time it takes for sge between the fact that a queued job is runnable and SGE actually starting it (I've put a long time here because sometimes, qstat takes a long time to ralise that the machine is available after I wake it up)
            log_info('ClusterController::wake_up_machinesForPendingJobs : all required machines are awake. Now give %d seconds to SGE to allocate slots.' % iSGE_CHEK_RUNNABLE_JOBS_DELAY)
            # wait until SGE has a chance to allocate slots
            time.sleep(iSGE_CHEK_RUNNABLE_JOBS_DELAY)  # note : this is annoying because it blocks the main thread. This could be improved if we forbid the machines to go to sleep for that much time....
            log_info('ClusterController::wake_up_machinesForPendingJobs : end of the delay given to SGE to allocate slots')

    def updateNormalState(self):
        # attempt to shut down machines that are idle
        self.putIdleMachinesToSleep()
        # wake up necessary machines if there are pending jobs
        self.wake_up_machinesForPendingJobs()

    def storeSessionInDatabase(self):
        conn = MySQLdb.connect('simpatix10', 'clusterctrl', '', 'clustercontroller')
        assert conn

        # retrieve the session id, as it's an auto_increment field
        sqlCommand = "SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'clustercontroller' AND TABLE_NAME = 'sessions_desc'"
        print(sqlCommand)
        conn.query(sqlCommand)
        r = conn.store_result()
        iSessionId = r.fetch_row()[0][0]

        # stores information about the session
        sqlCommand = "INSERT INTO `sessions_desc` (`start_time`, end_time, `program_version`, `machine_name`, `pid`, num_controlled_machines) VALUES (NOW(), NOW(), '%s', 'simpatix10', %d, %d);" % (VERSION, os.getpid(), len(self.cluster_status.cluster_nodes))
        print(sqlCommand)
        conn.query(sqlCommand)

        # initialize the energy savings table
        sqlCommand = "INSERT INTO session_to_energy_savings (session_id, energy_savings_kwh) VALUES (%d,0.0);" % (iSessionId)
        print(sqlCommand)
        conn.query(sqlCommand)

        conn.close()
        print('Session Iid = %d' % iSessionId)
        return iSessionId

    def updateSessionEnergyConsumptionInDatabase(self):
        conn = MySQLdb.connect('simpatix10', 'root', '', 'clustercontroller')
        assert conn

        # update energy savings for the current session
        sqlCommand = "UPDATE session_to_energy_savings SET energy_savings_kwh=%f WHERE session_id=%d;" % (jouleToKwh(self.cluster_status.get_energy_savings()), self.session_id)
        print(sqlCommand)
        conn.query(sqlCommand)

        # update the end time of the current session
        sqlCommand = "UPDATE sessions_desc SET end_time=NOW() WHERE session_id=%d;" % (self.session_id)
        print(sqlCommand)
        conn.query(sqlCommand)

        conn.close()

    def set_control_on_machine(self, machineName, bControl):
        """
            adds or removes the control of ClusterController on the given machine
        """
        self.cluster_status.set_control_on_machine(machineName, bControl)

    def run(self):
        """
        """
        self.session_id = self.storeSessionInDatabase()
        log("storeSessionInDatabase completed")
        DELAY_BETWEEN_MEASURES = 10  # in seconds
        self.cluster_status.start_reading_threads()
        self.web_server.start()
        while not self.cluster_status.is_ready():
            log('waiting for system to be ready')
            time.sleep(1)
        log_info('ClusterController::run : cluster initial readings have completed')
        startTime = time.localtime()
        while not self.stop:
            currentTime = time.time()
            if (not self.last_energy_status_log_time) or (currentTime > (self.last_energy_status_log_time + self.DELAY_BETWEEN_ENERGY_STATUS_LOGS)):
                iNumMachines = len(self.cluster_status.cluster_nodes)
                iNumMachinesOn = 0
                iNumSleepingMachines = 0
                for machine in self.cluster_status.cluster_nodes.values():
                    ePowerState = machine.get_power_state()
                    if ePowerState == PowerState.ON:
                        iNumMachinesOn += 1
                    elif ePowerState == PowerState.SLEEP:
                        iNumSleepingMachines += 1
                log_info('%d machines (%d ON, %d SLEEPING)' % (iNumMachines, iNumMachinesOn, iNumSleepingMachines))
                iNumSlots = self.cluster_status.get_num_controlled_slots()
                iNumUsedSlots = self.cluster_status.get_num_used_slots()
                iNumWastedSlots = self.cluster_status.get_num_wasted_slots()
                iNumSleepingSlots = self.cluster_status.get_num_sleeping_slots()
                log_info('%d slots (%d used, %d wasted, %d sleeping)' % (iNumSlots, iNumUsedSlots, iNumWastedSlots, iNumSleepingSlots))
                log_info('cluster estimated power consumption : %f W (saving from cluster controller : %f W)' % (self.cluster_status.get_current_power_consumption(), self.cluster_status.get_current_power_savings()))
                log_info('cluster estimated energy consumption since %s : %f kWh (saving from cluster controller : %f  kWh)' % (time.asctime(startTime), jouleToKwh(self.cluster_status.get_energy_consumption()), jouleToKwh(self.cluster_status.get_energy_savings())))
                self.updateSessionEnergyConsumptionInDatabase()
                self.last_energy_status_log_time = currentTime

            self.updateNormalState()
            time.sleep(DELAY_BETWEEN_MEASURES)
        self.cluster_status.stop_reading_threads()


def storeClusterNodeStatus(clusterNodeStatus):
    # conn = MySQLdb.connect('simpatix10', 'measures_writer', '', 'simpa_measurements')
    conn = MySQLdb.connect('simpatix10', 'root', '', 'simpa_measurements')
    assert conn
    # conn.query("""INSERT INTO `fan_rpm_logs` (`fan_id`, `rpm`, `date`) VALUES ('titi', 2000, NOW());""")
    # conn.query("""SELECT * FROM fan_rpm_logs""")
    # r=conn.store_result()
    # print r.fetch_row()[0]
    for _key, sensor in clusterNodeStatus.sensors.items():
        sensorId = clusterNodeStatus.cluster_node_name + '_' + sensor.name
        if sensor.typeName() == 'Fan':
            sqlCommand = """INSERT INTO `fan_rpm_logs` (`fan_id`, `rpm`, `date`) VALUES ('""" + sensorId + """', """ + str(sensor.rpms) + """, NOW());"""
            print(sqlCommand)
            conn.query(sqlCommand)
        elif sensor.typeName() == 'Temperature':
            sqlCommand = """INSERT INTO `temperature_logs` (`temp_sensor_id`, `temperature`, `date`) VALUES ('""" + sensorId + """', """ + str(sensor.temperature) + """, NOW());"""
            print(sqlCommand)
            conn.query(sqlCommand)
        else:
            assert False
    conn.close()


if __name__ == '__main__':
    # Lib.Util.send_text_mail('SimpaCluster <guillaume.raffy@univ-rennes1.fr>', 'guillaume.raffy@univ-rennes1.fr', 'mail subject', 'mail content')
    try:
        log_info('ClusterController v. %s starting....' % VERSION)
        # execute_command('ping -o -t 1 simpatix310 > /dev/null')
        # print execute_command('ssh simpatix10 "ipmitool sensor"')
        # assert  False, 'prout'
        controller = ClusterController()
        controller.run()
        # machineNameToMacAddress('simpatix10')
    # except AssertionError, error:
    # except KeyboardInterrupt, error:
    except BaseException as exception:  # catches all exceptions, including the ctrl+C (KeyboardInterrupt)
        on_exception(exception)
