import time
from .Util import execute_program
from .QstatParser import QstatParser
from .Log import logDebug, log_warning


class SunGridEngine:

    def get_current_job_state(self):
        bBUG_00000009_IS_STILL_ALIVE = True
        if bBUG_00000009_IS_STILL_ALIVE:
            logDebug('Querying the current state of jobs')
        return_code = -1
        delay_between_attempts = 5  # in seconds
        while return_code != 0:
            command = ['qstat', '-f', '-u', '*']
            (return_code, qstat_output, stderr) = execute_program(command)
            if return_code != 0:
                log_warning('command "%s" failed (returnCode = %d, stdout="%s", stderr="%s"). Retrying in %d seconds' % (' '.join(command), return_code, qstat_output, stderr, delay_between_attempts))
                time.sleep(delay_between_attempts)
        if bBUG_00000009_IS_STILL_ALIVE:
            logDebug('Just got current state of jobs')

        jobs_state = QstatParser().parse_qstat_output(qstat_output)
        jobs_state.set_time(time.time())

        # read the requirements for pending jobs (which parallel environment, which queue, which architecture) from sge
        if False:  # no need for job details at the moment and since it's very slow, it's been disabled
            for unused_jobId, job in jobs_state.get_pending_jobs().items():
                (return_code, stdout, stderr) = execute_program(['qstat', '-j', job.getId().asStr()])
                assert return_code != 0, 'prout'
                QstatParser().parse_job_details(stdout, job)

        return jobs_state

    def set_queue_instance_activation(self, queue_instance_name: str, enable: bool):
        argument = 'd'
        if enable:
            argument = 'e'
        bBUG_00000269_IS_STILL_ALIVE = True  # for some reason, qmod -d (and maybe any sge command) could fail with error: commlib error: can't connect to service (Address already in use)
        delay_between_attempts = 5  # in seconds
        while True:
            error_code, unused_stdout, unused_stderr = execute_program(['qmod', '-' + argument, queue_instance_name])
            if bBUG_00000269_IS_STILL_ALIVE:
                # if the command failed, try again
                if error_code == 0:
                    break
                time.sleep(delay_between_attempts)
            else:
                break
        return (error_code == 0)

    def queue_is_empty(self, machine_name: str):
        (returnCode, qstat_output, unused_stderr) = execute_program(['qstat', '-f', '-u', '*'])
        assert returnCode == 0
        jobs_state = QstatParser().parse_qstat_output(qstat_output)
        jobs = jobs_state.get_jobs_on_machine(machine_name)
        return (len(jobs) == 0)
