import io
import re
from .JobsState import JobsState
from .QueueMachine import QueueMachine, QueueMachineStateFlags
from .Log import logError
from .Job import JobStateFlags, TaskUid, Task, ParallelEnvironment, JobState
import logging
import time


class QstatParser:
    def parseJobState(self, job_status_as_str: str) -> JobState:
        job_state = 0
        for i in range(0, len(job_status_as_str)):
            c = job_status_as_str[i]
            if c == 'r':
                job_state += JobStateFlags.RUNNING
            elif c == 'w':
                job_state += JobStateFlags.WAITING
            elif c == 'q':
                job_state += JobStateFlags.QUEUED
            elif c == 't':
                job_state += JobStateFlags.TRANSFERING
            elif c == 'd':
                job_state += JobStateFlags.DELETED
            elif c == 'h':
                job_state += JobStateFlags.HOLD
            elif c == 's':
                job_state += JobStateFlags.SUSPENDED
            elif c == 'E':
                job_state += JobStateFlags.ERROR
            else:
                assert False, 'unhandled job state flag :"' + c + '"'
        return job_state

    def parseQueueMachineState(self, strQueueMachineStatus):
        queueMachineState = 0
        for i in range(0, len(strQueueMachineStatus)):
            c = strQueueMachineStatus[i]
            if c == 'd':
                queueMachineState += QueueMachineStateFlags.DISABLED
            elif c == 'a':
                queueMachineState += QueueMachineStateFlags.ALARM
            elif c == 'u':
                queueMachineState += QueueMachineStateFlags.UNKNOWN
            elif c == 'E':
                queueMachineState += QueueMachineStateFlags.ERROR
            elif c == 'o':
                queueMachineState += QueueMachineStateFlags.OBSOLETE
            elif c == 's':
                queueMachineState += QueueMachineStateFlags.SUSPENDED
            else:
                assert False, 'unhandled queue machine state flag :"' + c + '"'
        return queueMachineState

    def parse_qstat_output(self, qstat_output: str, cluster_domain: str = 'ipr.univ-rennes1.fr'):
        """
        parses result of command 'qstat -f -u \\* -pri'

        cluster_domain: network domain of the cluster (eg 'ipr.univ-rennes.fr'). This information is missing from qstat's output and is used to form the fully qualified domain name of the cluster machines.
        """
        logging.debug('qstatOutput type : %s', type(qstat_output))

        def parse_pending_tasks(task_ranges_sequence):
            """
            parses a job's task ids encoded in the form of a string containing a sequence of ranges

            :param str task_ranges_sequence: a job's task ids encoded in the form of a string containing a sequence of non overlapping ranges separated with a comma. Each range is expected to be in the form "<min_index>-<max_index>:<step>"
            :return list(int): the list of task ids

            for example, this function would return [1, 2, 3, 4, 6, 7, 8] for the input string "1-4:1,6-8:1"
            """
            task_ids = []
            ranges = re.split(',', task_ranges_sequence)
            for task_range in ranges:
                single_index_match = re.match('^(?P<elementIndex>[0-9]+)$', task_range)
                if single_index_match:
                    element_index = int(single_index_match.group('elementIndex'))
                    task_ids.extend(range(element_index, element_index + 1))
                else:
                    # we expect strRange to be of the form "1-4:1", where :
                    # the 1st number is the min element index (sge imposes it to be greater than 0)
                    # the 2nd number is the max element index
                    # the 3rd number is the step between consecutive element indices
                    range_match = re.match('^(?P<minElementIndex>[0-9]+)-(?P<maxElementIndex>[0-9]+):(?P<stepBetweenIndices>[0-9]+)$', task_range)
                    if range_match is None:
                        logError('unexpected format for job array details : "%s" (line="%s"' % (task_range, line))
                        assert False
                    min_element_index = int(range_match.group('minElementIndex'))
                    min_element_index = int(range_match.group('maxElementIndex'))
                    step_between_indices = int(range_match.group('stepBetweenIndices'))
                    task_ids.extend(range(min_element_index, min_element_index + 1, step_between_indices))
            return task_ids

        # ugly hack to work around the fact that qstat truncates the fqdn of cluster nodes
        # graffy@physix-master:~$ qstat -f -u \*
        # queuename                      qtype resv/used/tot. load_avg arch          states
        # ---------------------------------------------------------------------------------
        # main.q@physix88.ipr.univ-renne BIP   0/0/36         14.03    lx-amd64
        # TODO: fix this properly by parsing the output of 'qstat -f -u \* -xml' instead of 'qstat -f -u \*'
        qstat_output = re.sub(r'\.ipr\.univ[^ ]*', f'.{cluster_domain}', qstat_output)

        jobsState = JobsState()
        f = io.StringIO(qstat_output)
        line = f.readline()
        current_queue_machine = None
        in_pending_jobs_section = False
        # examples of job line :
        #   43521 0.55108 Confidiso3 aghoufi      r     08/19/2009 18:40:09     1
        # a typical job line in the pending jobs section looks like this :
        #   43645 0.00000 LC_LV_MC   aghoufi      qw    08/21/2009 08:14:58     1
        # a typical running job array line looks like this
        #   43619 0.56000 SimpleJobA raffy        r     08/20/2009 18:13:03     1 3
        # a typical job array line in the pending jobs section looks like this
        #   43646 0.00000 SimpleJobA raffy        qw    08/21/2009 09:56:40     1 1-4:1

        # nurg   The job's total urgency value in normalized fashion.
        # npprior The job's -p priority in normalized fashion.
        # ntckts The job's ticket amount in normalized fashion.
        # ppri   The job's -p priority as specified by the user.

        job_regular_exp = re.compile(r'^[ ]*(?P<jobId>[^ ]+)[ ]+(?P<JobPriority>[0-9.]+)[ ]+(?P<nurg>[0-9.]+)[ ]+(?P<npprior>[0-9.]+)[ ]+(?P<ntckts>[0-9.]+)[ ]+(?P<ppri>-?[0-9]+)[ ]+(?P<jobScriptName>[^ ]+)[ ]+(?P<jobOwner>[^ ]+)[ ]+(?P<jobStatus>[^ ]+)[ ]+(?P<jobStartOrSubmitTime>[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9])[ ]+(?P<numSlots>[0-9]+)[ ]+(?P<jobArrayDetails>[^\n]*)[\s]*$')
        # example of machine line :
        # allintel.q@simpatix34.univ-ren BIP   0/6/8          6.00     darwin-x86
        machine_regular_exp = re.compile(r'^(?P<queueName>[^@]+)@(?P<machineName>[^ ]+)[ ]+(?P<queueTypeString>[^ ]+)[ ]+(?P<numReservedSlots>[^/]+)/(?P<numUsedSlots>[^/]+)/(?P<numTotalSlots>[^ ]+)[ ]+(?P<cpuLoad>[^ ]+)[\s]+(?P<archName>[^ ]+)[\s]+(?P<queueMachineStatus>[^\s]*)')
        pending_jobs_header_regular_exp = re.compile('^ - PENDING JOBS - PENDING JOBS - PENDING JOBS - PENDING JOBS - PENDING JOBS[?]*')
        while len(line) > 0:
            # print line
            # check if the current line is a line describing a job running on a machine
            match_obj = job_regular_exp.match(line)
            if match_obj:
                # we are dealing with a job line
                if not in_pending_jobs_section:
                    assert current_queue_machine
                # log('QstatParser::parseQstatOutput : jobId = "'+matchObj.group('jobId')+'"')
                job_id = int(match_obj.group('jobId'))
                logging.debug('iJobId = %d', job_id)
                job_state = self.parseJobState(match_obj.group('jobStatus'))
                job_array_details = match_obj.group('jobArrayDetails')
                is_job_array = (len(job_array_details) != 0)
                # logDebug('strJobArrayDetails = "%s", bIsJobArray=%d' % (strJobArrayDetails, int(bIsJobArray)))
                # each element of a job array is treated as a separate job for the sake of simplicity.
                # For these elements, the job id in sge sense is the same, but they are different in this program's sense
                task_ids = range(0, 1)  # just one element, unless it's a job array
                if is_job_array:
                    if in_pending_jobs_section:
                        task_ids = parse_pending_tasks(job_array_details)
                    else:
                        # we are in the running jobs section, and here we expect the strJobArrayDetails to just contain the index of the job array element
                        task_id = int(job_array_details)
                        assert task_id != 0  # sge does not allow element indices to be 0
                        task_ids = range(task_id, task_id + 1)
                logging.debug('task_ids = %s', task_ids)
                for task_id in task_ids:
                    logging.debug('task_id = %s', task_id)
                    task_uid = None
                    if is_job_array:
                        task_uid = TaskUid(job_id, task_id)
                    else:
                        task_uid = TaskUid(job_id)
                    task = jobsState.get_task(task_uid)
                    # logDebug('iElementIndex = %d job id = %s' % (iElementIndex, jobId.asStr()))
                    if task is None:
                        # this job hasn't been encountered yet in the output of qstat ...
                        # we could either be in the pending jobs section or in the running jobs section
                        task = Task(task_uid)
                        jobsState.add_task(task)
                        task.set_state(job_state)
                        job_start_or_submit_time_as_str = match_obj.group('jobStartOrSubmitTime')
                        job_start_or_submit_time = time.strptime(job_start_or_submit_time_as_str, '%m/%d/%Y %H:%M:%S')
                        if in_pending_jobs_section:
                            task.get_submit_time(job_start_or_submit_time)
                        else:
                            task.set_start_time(job_start_or_submit_time)
                        task.set_owner(match_obj.group('jobOwner'))
                        task.set_script_name(match_obj.group('jobScriptName'))
                        if in_pending_jobs_section:
                            task.set_num_required_slots(int(match_obj.group('numSlots')))
                    else:
                        assert not in_pending_jobs_section  # if we are in the pending jobs section, the job should be new
                    if not in_pending_jobs_section:
                        task.add_slots(current_queue_machine.get_name(), int(match_obj.group('numSlots')))
            else:
                # the current line does not describe a job
                if not in_pending_jobs_section:
                    # check if this line describes the status of a machine
                    match_obj = machine_regular_exp.match(line)
                    if match_obj:
                        queue_name = match_obj.group('queueName')
                        machine_name = match_obj.group('machineName')
                        queue_machine = QueueMachine(queue_name, machine_name)
                        # log(line)
                        # log('matchObj.group(queueTypeString) :' + matchObj.group('queueTypeString'))
                        # log('matchObj.group(numTotalSlots) :' + matchObj.group('numTotalSlots'))
                        queue_machine.set_num_slots(int(match_obj.group('numTotalSlots')))
                        queue_machine.set_num_used_slots(int(match_obj.group('numUsedSlots')))
                        cpu_load_as_str = match_obj.group('cpuLoad')
                        if cpu_load_as_str != '-NA-':
                            queue_machine.set_cpu_load(float(cpu_load_as_str))

                        queue_machine_state_as_str = match_obj.group('queueMachineStatus')
                        queue_machine.set_state(self.parseQueueMachineState(queue_machine_state_as_str))
                        # log('QstatParser::parseQstatOutput : queueName = "'+matchObj.group('queueName')+'"')
                        # log('QstatParser::parseQstatOutput : machineName = "'+matchObj.group('machineName')+'"')
                        current_queue_machine = queue_machine
                        jobsState.add_queue_machine(queue_machine)
                    else:
                        match_obj = pending_jobs_header_regular_exp.match(line)
                        if match_obj:
                            in_pending_jobs_section = True
                            current_queue_machine = None
                        else:
                            pass
                else:
                    # we are in a pending jobs section
                    match_obj = re.match('^[#]+$', line)
                    if not match_obj:
                        # unexpected line
                        print('line = "' + line + '"')
                        assert False
            line = f.readline()
        f.close()
        return jobsState

    def parse_job_details(self, qstat_output: str, task: Task):
        """
            adds to job the details parsed from the output of the "qstat -j <jobid>" command
        """
        f = io.StringIO(qstat_output)
        line = f.readline()
        fieldRegularExp = re.compile('^(?P<fieldName>[^:]+):[ ]+(?P<fieldValue>[?]*)$')
        while len(line) > 0:
            # print line
            # check if the current line is a line describing a job running on a machine
            matchObj = fieldRegularExp.match(line)
            if matchObj:
                fieldName = matchObj.group('fieldName')
                strFieldValue = matchObj.group('fieldValue')
                if fieldName == 'job_number':
                    assert task.getId().asStr() == strFieldValue
                elif fieldName == 'hard_queue_list':
                    allowedQueues = strFieldValue.split(',')
                    assert len(allowedQueues) > 0
                    task.job_requirements.queues = allowedQueues
                elif fieldName == 'parallel environment':
                    # the value could be 'ompi range: 32'
                    matchObj = re.match('ompi range: (?P<numSlots>[0-9]+)[?]*', strFieldValue)
                    if matchObj:
                        task.job_requirements.parallel_environment = ParallelEnvironment.MPI
                    else:
                        assert False
                else:
                    # ignore the other fields
                    pass
            line = f.readline()
        f.close()
