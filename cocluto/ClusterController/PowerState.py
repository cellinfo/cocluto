
import enum


class PowerState(enum.Enum):
    UNKNOWN = 0
    OFF = 1
    ON = 2
    SLEEP = 3
    UNPLUGGED = 4


def PowerStateToStr(powerState):
    if powerState == PowerState.UNKNOWN:
        return 'UNKNOWN'
    if powerState == PowerState.OFF:
        return 'OFF'
    if powerState == PowerState.ON:
        return 'ON'
    if powerState == PowerState.SLEEP:
        return 'SLEEP'
    if powerState == PowerState.UNPLUGGED:
        return 'UNPLUGGED'
    else:
        assert False
