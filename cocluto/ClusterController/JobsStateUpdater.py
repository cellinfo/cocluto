from typing import TYPE_CHECKING
import threading
import time
from .Util import on_exception
if TYPE_CHECKING:
    from .ClusterStatus import ClusterStatus


class JobsStateUpdater(threading.Thread):
    cluster_status: 'ClusterStatus'
    stop: bool
    DELAY_BETWEEN_STATUS_CHECKS = 10  # in seconds

    def __init__(self, clusterStatus):
        threading.Thread.__init__(self)
        self.cluster_status = clusterStatus
        self.stop = False

    def get_name(self):
        return 'JobsStateUpdater'

    def get_grid_engine(self):
        return self.cluster_status.get_grid_engine()

    def updateClusterStatus(self):
        # log('JobsStateUpdater::updateClusterStatus : start')

        jobsState = self.get_grid_engine().getCurrentJobsState()
        # update the jobs in the cluster status
        self.cluster_status.on_new_jobs_state(jobsState)
        # log('JobsStateUpdater::updateClusterStatus : end')

    def run(self):
        try:
            while not self.stop:
                self.updateClusterStatus()
                time.sleep(JobsStateUpdater.DELAY_BETWEEN_STATUS_CHECKS)
        except BaseException as exception:  # catches all exceptions, including the ctrl+C (KeyboardInterrupt)
            on_exception(exception)
