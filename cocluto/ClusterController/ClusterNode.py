from typing import TYPE_CHECKING
from typing import Optional
from .PowerState import PowerState, PowerStateToStr
from .Log import log_info, log_warning
from .ClusterNodeStatusUpdater import ClusterNodeStatusUpdater
if TYPE_CHECKING:
    from .ClusterStatus import ClusterStatus
    from .SunGridEngine import SunGridEngine
    from .ClusterController import SleepCompleteNotifier

from datetime import datetime

ClusterNodeId = str  # eg 'physix99'


class ClusterNode:
    """
        the state of a machine node
    """
    name: ClusterNodeId
    cluster: 'ClusterStatus'  # the cluster this machine belongs to
    requested_power_state: PowerState
    power_state: PowerState
    last_power_state_time: Optional[datetime]  # time at which the last value of self.power_state has been set
    machine_status_updater: ClusterNodeStatusUpdater
    energy_consumption: float  # estimate of the energy consumption of this machine since the start of cluster controller (in joules)
    energy_savings: float  # estimate of the energy savings on this machine caused by the cluster controller since it started (in joules)

    def __init__(self, machine_name: ClusterNodeId, cluster: 'ClusterStatus', grid_engine: 'SunGridEngine'):
        self.name = machine_name
        self.cluster = cluster        # the cluster this machine belongs to
        self.requested_power_state = PowerState.ON
        self.power_state = PowerState.UNKNOWN
        self.last_power_state_time = None  # time at which the last value of self.power_state has been set
        self.machine_status_updater = ClusterNodeStatusUpdater(machine_name, self, grid_engine)
        self.energy_consumption = 0.0  # estimate of the energy consumption of this machine since the start of cluster controller (in joules)
        self.energy_savings = 0.0  # estimate of the energy savings on this machine caused by the cluster controller since it started (in joules)

    def get_name(self) -> ClusterNodeId:
        return self.name

    def is_ready(self) -> bool:
        if self.power_state == PowerState.UNKNOWN:
            # log_info(self.name + ' is not ready (waiting for power state)')
            return False
        if self.power_state == PowerState.ON:
            return True
        # log(self.name + ' is ready')
        return True

    def get_power_state(self) -> PowerState:
        return self.power_state

    def set_should_always_be_on(self):
        self.machine_status_updater.set_should_always_be_on()
        self.set_power_state(PowerState.ON)

    def set_power_state(self, power_state: PowerState):
        bUpdateRequiredChecks = False
        if self.power_state == PowerState.UNKNOWN:
            log_info('ClusterNode::set_power_state : ' + self.name + '\'s power state has been initialized to ' + PowerStateToStr(power_state))
            self.power_state = power_state
            self.last_power_state_time = datetime.now()
            bUpdateRequiredChecks = True
        else:
            # update the estimation of energy consumption
            self.update_energy_measurements()
            # then change the power state
            if self.power_state != power_state:
                log_info('ClusterNode::set_power_state : ' + self.name + '\'s power state has been changed to ' + PowerStateToStr(power_state))
                self.power_state = power_state
                self.last_power_state_time = datetime.now()
            bUpdateRequiredChecks = True
        if bUpdateRequiredChecks:
            if self.power_state == PowerState.ON:
                self.machine_status_updater.check_power_state = True
                self.machine_status_updater.check_sensors = True
            elif self.power_state == PowerState.OFF:
                self.machine_status_updater.check_power_state = True
                self.machine_status_updater.check_sensors = False
            elif self.power_state == PowerState.SLEEP:
                self.machine_status_updater.check_power_state = True
                self.machine_status_updater.check_sensors = False
            elif self.power_state == PowerState.UNPLUGGED:
                self.machine_status_updater.check_power_state = True
                self.machine_status_updater.check_sensors = False
            else:
                assert False

    def on_new_power_state_reading(self, power_state: PowerState):
        """
            called when a new powerstate reading arrives
        """
        if power_state != self.get_power_state():
            if self.get_power_state() != PowerState.UNKNOWN:
                log_warning('ClusterNode::on_new_power_state_reading : ' + self.name + '\'s power state has been (manually it seems) changed to ' + PowerStateToStr(power_state))
            self.set_power_state(power_state)

    def get_power_consumption_for_power_state(self, power_state: PowerState) -> float:
        """
            returns the power consumption estimation (in watts) of this machine for the given power state
        """
        fCurrentIntensity = 0.0
        fCurrentVoltage = 220.0
        # noticed on 26.08.2009 that putting 22 machines from sleep to on eats 17 A, resulting in difference of 0.77 A per machine
        if power_state == PowerState.ON:
            fCurrentIntensity = 0.9    # value when the machine is doing nothing
        elif power_state == PowerState.OFF:
            fCurrentIntensity = 0.1
        elif power_state == PowerState.SLEEP:
            fCurrentIntensity = 0.1
        elif power_state == PowerState.UNPLUGGED:
            fCurrentIntensity = 0.0
        else:
            assert False
        return fCurrentIntensity * fCurrentVoltage

    def update_energy_measurements(self):
        timeInterval = datetime.now() - self.last_power_state_time
        self.energy_consumption += self.get_power_consumption_for_power_state(self.power_state) * timeInterval.seconds
        self.energy_savings += (self.get_power_consumption_for_power_state(PowerState.ON) - self.get_power_consumption_for_power_state(self.power_state)) * timeInterval.seconds
        self.last_power_state_time = datetime.now()
        # logDebug('energy savings on %s : %f J' %(self.get_name(), self.energy_savings))

    def get_energy_consumption(self) -> float:
        """
            in joules
        """
        self.update_energy_measurements()
        return self.energy_consumption

    def get_power_consumption(self) -> float:
        fCurrentPowerConsumption = self.get_power_consumption_for_power_state(self.power_state)
        # logDebug('get_power_consumption of %s : %f (powerstate = %d)' % (self.get_name(), fCurrentPowerConsumption, self.power_state))
        return fCurrentPowerConsumption

    def get_energy_savings(self) -> float:
        self.update_energy_measurements()
        return self.energy_savings

    def on_sleep_because_a_job_just_arrived(self):
        log_info('%s was scheduled to sleep but the sleep is canceled because it\'s currently executing a new job' % self.name)

    def request_sleep(self, sleep_complete_notifier: Optional['SleepCompleteNotifier'] = None):
        self.machine_status_updater.request_sleep(sleep_complete_notifier)

    def request_wake_up(self, wake_up_complete_notifier: Optional['SleepCompleteNotifier'] = None):
        self.machine_status_updater.request_wake_up(wake_up_complete_notifier)

    def get_queue_machine_name(self) -> ClusterNodeId:
        return self.get_cluster().get_jobs_state().get_queue_machine(self.name).get_name()
        # assert self.queue_name is not None
        # return self.queue_name

    def get_cluster(self) -> 'ClusterStatus':
        return self.cluster


# from .ClusterStatus import ClusterStatus  # noqa: E402, pylint: disable=wrong-import-position
# from .SunGridEngine import SunGridEngine  # noqa: E402, pylint: disable=wrong-import-position
# from .ClusterController import SleepCompleteNotifier  # noqa: E402, pylint: disable=wrong-import-position
