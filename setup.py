from setuptools import setup

setup(
    name='cocluto',
    version=1.07,
    description='compute cluster utility tools',
    url='https://git.ipr.univ-rennes1.fr/graffy/cocluto',
    author='Guillaume Raffy',
    author_email='guillaume.raffy@univ-rennes1.fr',
    license='MIT',
    packages=['cocluto'],
    install_requires=['pygraphviz', 'mysqlclient'],  # requires apt install graphviz-dev
    zip_safe=False)
